<?php 
/*----------------------------------------------------------------*\

	Template Name: Form
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<main>

	<article>
		<section class="wysiwyg-block">
			<?php the_field('form'); ?>
		</section>
		<section class="wysiwyg-block">
			<?php the_field('article'); ?>
		</section>
	</article>
	
</main>

<?php get_template_part('template-parts/sections/newsletter'); ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>