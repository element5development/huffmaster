<?php 
/*----------------------------------------------------------------*\

	Template Name: Licenses
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<main>

	<article>
		<section class="licenses">
			<?php while ( have_rows('licenses') ) : the_row(); ?>
				<div class="license">
					<div>
						<p><?php the_sub_field('state'); ?></p>
						<?php $date = get_sub_field('expire_date', false, false); $date = new DateTime($date); ?>
						<p>Expires: <?php echo $date->format('n/j/Y'); ?></p>
					</div>
					<div>
						<p><?php the_sub_field('pre-title'); ?></p>
						<h3><?php the_sub_field('title'); ?></h3>
						<p><?php the_sub_field('llc'); ?></p>
					</div>
				</div>
    	<?php endwhile; ?>
		</section>
	</article>
	
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>