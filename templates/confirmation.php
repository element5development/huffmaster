<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main>
	<article>

		<?php 
			if( have_rows('article') ):
				?>
				<section class="cards-block">
					<h1>
						<?php 
							if ( get_field('page_title') ) :
								the_field('page_title');
							else :
								the_title();
							endif;
						?>
					</h2>
					<?php if ( get_field('title_description') ) : ?>
						<p>
							<?php the_field('title_description'); ?>
						</p>
					<?php endif; ?>
				</section>
				<?php 
				$id = 0;
				while ( have_rows('article') ) : the_row(); 
					$id++;
					if( get_row_layout() == 'wysiwyg' ):
						hm_get_template_part('template-parts/sections/wysiwyg', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'wysiwyg-two-col' ):
						hm_get_template_part('template-parts/sections/wysiwyg-two-col', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'cards' ): 
						hm_get_template_part('template-parts/sections/cards', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'link_cards' ): 
						hm_get_template_part('template-parts/sections/link-cards', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'logo_cards' ): 
						hm_get_template_part('template-parts/sections/logo-cards', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'icon_cards' ):
						hm_get_template_part('template-parts/sections/icon-cards', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'icon_links' ): 
						hm_get_template_part('template-parts/sections/icon-links', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'gallery' ): 
						hm_get_template_part('template-parts/sections/gallery', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'testimonials' ): 
						hm_get_template_part('template-parts/sections/testimonials', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'news_&_events_feed' ): 
						hm_get_template_part('template-parts/sections/news-events', [ 'sectionId' => $id ]);
					elseif( get_row_layout() == 'team_members' ): 
						get_template_part('template-parts/sections/team');
					elseif( get_row_layout() == 'newsletter' ): 
						hm_get_template_part('template-parts/sections/newsletter', [ 'sectionId' => $id ]);
					endif;
		
				endwhile;
			else : 
		?>
			<section class="cards-block">
				<h1>
					<?php 
						if ( get_field('page_title') ) :
							the_field('page_title');
						else :
							the_title();
						endif;
					?>
				</h2>
				<?php if ( get_field('title_description') ) : ?>
					<p>
						<?php the_field('title_description'); ?>
					</p>
				<?php endif; ?>
				<div class="cards">
					<div class="card">
						<h3>Interested in upcoming events or the latest news?</h3>
						<div class="button">News & Events</div>
						<a href="<?php the_permalink(102); ?>"></a>
					</div>
					<div class="card">
						<h3>Just want to go back to the homepage?</h3>
						<div class="button">Return Home</div>
						<a href="<?php echo get_site_url(); ?>"></a>
					</div>
					<div class="card">
						<h3>Need Immediate Strike Services?</h3>
						<div class="button">+1 (800) 446-1515</div>
						<a href="tel:+18004461515"></a>
					</div>
				</div>
			</section>
		<?php endif; ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/newsletter'); ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>