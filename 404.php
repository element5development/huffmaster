<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main>
	<article>
		<section class="cards-block">
			<h1>Page Not Found</h1>
			<p>The page you were looking for appears to have been moved, deleted or does not exist.</p>
			<div class="cards">
				<div class="card">
					<h3>Interested in upcoming events or the latest news?</h3>
					<div class="button">News & Events</div>
					<a href="<?php the_permalink(102); ?>"></a>
				</div>
				<div class="card">
					<h3>Just want to go back to the homepage?</h3>
					<div class="button">Return Home</div>
					<a href="<?php echo get_site_url(); ?>"></a>
				</div>
				<div class="card">
					<h3>Need Immediate Strike Services?</h3>
					<div class="button">+1 (800) 446-1515</div>
					<a href="tel:+18004461515"></a>
				</div>
			</div>
		</section>
		<section class="wysiwyg-block">
			<h2>Search our site</h2>
			<?php echo get_search_form(); ?>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>