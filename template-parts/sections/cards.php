<?php 
/*----------------------------------------------------------------*\

	CARD GRID

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="cards-block">
	<h2><?php the_sub_field('headline'); ?></h2>
	<?php if( have_rows('cards') ): ?>
		<div class="cards">
			<?php while ( have_rows('cards') ) : the_row(); ?>

				<div class="card <?php if ( get_sub_field('icon') ) : ?>has-image<?php endif ?>">
					<?php $link = get_sub_field('link'); ?>
					<?php if ( get_sub_field('icon') ) : ?>
						<?php $icon = get_sub_field('icon'); ?>
						<div class="icon">
							<img src="<?php echo $icon['sizes']['small']; ?>" alt="<?php echo $icon['alt']; ?>" />
						</div>
						<h3><?php the_sub_field('title'); ?></h3>
					<?php else : ?>
						<h3><?php the_sub_field('title'); ?></h3>
						<div class="button"><?php echo $link['title']; ?></div>
					<?php endif; ?>
					<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"></a>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>