<?php 
/*----------------------------------------------------------------*\

	NEWSLETTER

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="newsletter">
	<div>
		<h2>Stay Informed</h2>
		<?php echo do_shortcode('[gravityform id="4" title="false" description="false"]'); ?>
	</div>
</section>