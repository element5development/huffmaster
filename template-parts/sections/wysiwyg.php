<?php 
/*----------------------------------------------------------------*\

	CLASSIC WYSIWYG EDITOR

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="wysiwyg-block">
	<?php the_sub_field('wysiwyg'); ?>
</section>