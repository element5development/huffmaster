<?php 
/*----------------------------------------------------------------*\

	DEFAULT FOOTER
	Made up of widget areas
	May need to add or remove additional areas depending on the design

\*----------------------------------------------------------------*/
?>

<footer>
	<section>
		<div>
			<?php dynamic_sidebar( 'first-footer' ); ?>
		</div>
		<div>
			<?php dynamic_sidebar( 'second-footer' ); ?>
		</div>
		<div>
			<?php dynamic_sidebar( 'third-footer' ); ?>
		</div>
		<div>
			<?php dynamic_sidebar( 'fourth-footer' ); ?>
		</div>
		<div>
			<?php dynamic_sidebar( 'fifth-footer' ); ?>
		</div>
	</section>
	<div class="copyright">
		<div id="element5-credit" data-emergence="hidden">
			<a target="_blank" href="https://element5digital.com">
				<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
			</a>
		</div>
		<section class="legal">
			<div>
				<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p> 
				<?php wp_nav_menu(array( 'theme_location' => 'legal_nav' )); ?>
			</div>
			<?php if ( is_page(401) || in_array("401", get_ancestors( get_the_ID(), 'page' )) || is_page(398) ) : ?>
				<nav>
					<? if ( get_field('healthcare_strikes_facebook', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('healthcare_strikes_facebook', 'option'); ?>" title="Connect on Facebook">
							<svg viewBox="0 0 48 48"><path d="M19.33 46V25.93h-6.755v-7.822h6.754v-5.769C19.33 5.645 23.418 2 29.39 2c2.861 0 5.32.213 6.036.308v6.996l-4.142.002c-3.248 0-3.876 1.543-3.876 3.808v4.994h7.745l-1.008 7.822h-6.737V46h-8.078z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('healthcare_strikes_twitter', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('healthcare_strikes_twitter', 'option'); ?>" title="Connect on Twitter">
							<svg viewBox="0 0 48 48"><path d="M48 9.1c-1.8.8-3.7 1.3-5.7 1.6 2-1.2 3.6-3.1 4.3-5.4-1.9 1.1-4 1.9-6.3 2.4-1.8-1.9-4.4-3.1-7.2-3.1-5.4 0-9.8 4.4-9.8 9.8 0 .8.1 1.5.3 2.2-8.1-.4-15.4-4.3-20.3-10.3C2.5 7.8 2 9.4 2 11.2c0 3.4 1.7 6.4 4.4 8.2-1.6-.1-3.1-.5-4.5-1.2v.1c0 4.8 3.4 8.8 7.9 9.7-.8.2-1.7.3-2.6.3-.6 0-1.3-.1-1.9-.2 1.3 3.9 4.9 6.8 9.2 6.8-3.4 2.6-7.6 4.2-12.2 4.2-.8 0-1.6 0-2.3-.1 4.4 2.8 9.5 4.4 15.1 4.4 18.1 0 28-15 28-28v-1.3c1.9-1.3 3.6-3 4.9-5z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('healthcare_strikes_linkedin', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('healthcare_strikes_linkedin', 'option'); ?>" title="Connect on Linkedin">
							<svg viewBox="0 0 48 48"><path d="M46 0H2C.9 0 0 .9 0 2v44c0 1.1.9 2 2 2h44c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2zM14.2 40.9H7.1V18h7.1v22.9zm-3.5-26c-2.3 0-4.1-1.8-4.1-4.1s1.8-4.1 4.1-4.1 4.1 1.8 4.1 4.1c0 2.2-1.8 4.1-4.1 4.1zm30.2 26h-7.1V29.8c0-2.7 0-6.1-3.7-6.1s-4.3 2.9-4.3 5.9v11.3h-7.1V18h6.8v3.1h.1c.9-1.8 3.3-3.7 6.7-3.7 7.2 0 8.5 4.7 8.5 10.9v12.6z"/></svg>
						</a>
					<?php endif; ?>
				</nav>
			<?php elseif ( is_page(428) ||  in_array("428", get_ancestors( get_the_ID(), 'page' )) || is_page(399) ) : ?>
				<nav>
					<? if ( get_field('industrial_staffing_facebook', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('industrial_staffing_facebook', 'option'); ?>" title="Connect on Facebook">
							<svg viewBox="0 0 48 48"><path d="M19.33 46V25.93h-6.755v-7.822h6.754v-5.769C19.33 5.645 23.418 2 29.39 2c2.861 0 5.32.213 6.036.308v6.996l-4.142.002c-3.248 0-3.876 1.543-3.876 3.808v4.994h7.745l-1.008 7.822h-6.737V46h-8.078z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('industrial_staffing_twitter', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('industrial_staffing_twitter', 'option'); ?>" title="Connect on Twitter">
							<svg viewBox="0 0 48 48"><path d="M48 9.1c-1.8.8-3.7 1.3-5.7 1.6 2-1.2 3.6-3.1 4.3-5.4-1.9 1.1-4 1.9-6.3 2.4-1.8-1.9-4.4-3.1-7.2-3.1-5.4 0-9.8 4.4-9.8 9.8 0 .8.1 1.5.3 2.2-8.1-.4-15.4-4.3-20.3-10.3C2.5 7.8 2 9.4 2 11.2c0 3.4 1.7 6.4 4.4 8.2-1.6-.1-3.1-.5-4.5-1.2v.1c0 4.8 3.4 8.8 7.9 9.7-.8.2-1.7.3-2.6.3-.6 0-1.3-.1-1.9-.2 1.3 3.9 4.9 6.8 9.2 6.8-3.4 2.6-7.6 4.2-12.2 4.2-.8 0-1.6 0-2.3-.1 4.4 2.8 9.5 4.4 15.1 4.4 18.1 0 28-15 28-28v-1.3c1.9-1.3 3.6-3 4.9-5z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('industrial_staffing_linkedin', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('industrial_staffing_linkedin', 'option'); ?>" title="Connect on Linkedin">
							<svg viewBox="0 0 48 48"><path d="M46 0H2C.9 0 0 .9 0 2v44c0 1.1.9 2 2 2h44c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2zM14.2 40.9H7.1V18h7.1v22.9zm-3.5-26c-2.3 0-4.1-1.8-4.1-4.1s1.8-4.1 4.1-4.1 4.1 1.8 4.1 4.1c0 2.2-1.8 4.1-4.1 4.1zm30.2 26h-7.1V29.8c0-2.7 0-6.1-3.7-6.1s-4.3 2.9-4.3 5.9v11.3h-7.1V18h6.8v3.1h.1c.9-1.8 3.3-3.7 6.7-3.7 7.2 0 8.5 4.7 8.5 10.9v12.6z"/></svg>
						</a>
					<?php endif; ?>
				</nav>
			<?php elseif ( is_page(434) ||  in_array("434", get_ancestors( get_the_ID(), 'page' )) || in_array("400", get_ancestors( get_the_ID(), 'page' ))  ) : ?>
				<nav>
					<? if ( get_field('security_services_facebook', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('security_services_facebook', 'option'); ?>" title="Connect on Facebook">
							<svg viewBox="0 0 48 48"><path d="M19.33 46V25.93h-6.755v-7.822h6.754v-5.769C19.33 5.645 23.418 2 29.39 2c2.861 0 5.32.213 6.036.308v6.996l-4.142.002c-3.248 0-3.876 1.543-3.876 3.808v4.994h7.745l-1.008 7.822h-6.737V46h-8.078z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('security_services_twitter', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('security_services_twitter', 'option'); ?>" title="Connect on Twitter">
							<svg viewBox="0 0 48 48"><path d="M48 9.1c-1.8.8-3.7 1.3-5.7 1.6 2-1.2 3.6-3.1 4.3-5.4-1.9 1.1-4 1.9-6.3 2.4-1.8-1.9-4.4-3.1-7.2-3.1-5.4 0-9.8 4.4-9.8 9.8 0 .8.1 1.5.3 2.2-8.1-.4-15.4-4.3-20.3-10.3C2.5 7.8 2 9.4 2 11.2c0 3.4 1.7 6.4 4.4 8.2-1.6-.1-3.1-.5-4.5-1.2v.1c0 4.8 3.4 8.8 7.9 9.7-.8.2-1.7.3-2.6.3-.6 0-1.3-.1-1.9-.2 1.3 3.9 4.9 6.8 9.2 6.8-3.4 2.6-7.6 4.2-12.2 4.2-.8 0-1.6 0-2.3-.1 4.4 2.8 9.5 4.4 15.1 4.4 18.1 0 28-15 28-28v-1.3c1.9-1.3 3.6-3 4.9-5z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('security_services_linkedin', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('security_services_linkedin', 'option'); ?>" title="Connect on Linkedin">
							<svg viewBox="0 0 48 48"><path d="M46 0H2C.9 0 0 .9 0 2v44c0 1.1.9 2 2 2h44c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2zM14.2 40.9H7.1V18h7.1v22.9zm-3.5-26c-2.3 0-4.1-1.8-4.1-4.1s1.8-4.1 4.1-4.1 4.1 1.8 4.1 4.1c0 2.2-1.8 4.1-4.1 4.1zm30.2 26h-7.1V29.8c0-2.7 0-6.1-3.7-6.1s-4.3 2.9-4.3 5.9v11.3h-7.1V18h6.8v3.1h.1c.9-1.8 3.3-3.7 6.7-3.7 7.2 0 8.5 4.7 8.5 10.9v12.6z"/></svg>
						</a>
					<?php endif; ?>
				</nav>
			<?php elseif ( is_page(422) ||  in_array("422", get_ancestors( get_the_ID(), 'page' )) || is_page(400) ) : ?>
				<nav>
					<? if ( get_field('strike_services_facebook', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('strike_services_facebook', 'option'); ?>" title="Connect on Facebook">
							<svg viewBox="0 0 48 48"><path d="M19.33 46V25.93h-6.755v-7.822h6.754v-5.769C19.33 5.645 23.418 2 29.39 2c2.861 0 5.32.213 6.036.308v6.996l-4.142.002c-3.248 0-3.876 1.543-3.876 3.808v4.994h7.745l-1.008 7.822h-6.737V46h-8.078z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('strike_services_twitter', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('strike_services_twitter', 'option'); ?>" title="Connect on Twitter">
							<svg viewBox="0 0 48 48"><path d="M48 9.1c-1.8.8-3.7 1.3-5.7 1.6 2-1.2 3.6-3.1 4.3-5.4-1.9 1.1-4 1.9-6.3 2.4-1.8-1.9-4.4-3.1-7.2-3.1-5.4 0-9.8 4.4-9.8 9.8 0 .8.1 1.5.3 2.2-8.1-.4-15.4-4.3-20.3-10.3C2.5 7.8 2 9.4 2 11.2c0 3.4 1.7 6.4 4.4 8.2-1.6-.1-3.1-.5-4.5-1.2v.1c0 4.8 3.4 8.8 7.9 9.7-.8.2-1.7.3-2.6.3-.6 0-1.3-.1-1.9-.2 1.3 3.9 4.9 6.8 9.2 6.8-3.4 2.6-7.6 4.2-12.2 4.2-.8 0-1.6 0-2.3-.1 4.4 2.8 9.5 4.4 15.1 4.4 18.1 0 28-15 28-28v-1.3c1.9-1.3 3.6-3 4.9-5z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('strike_services_linkedin', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('strike_services_linkedin', 'option'); ?>" title="Connect on Linkedin">
							<svg viewBox="0 0 48 48"><path d="M46 0H2C.9 0 0 .9 0 2v44c0 1.1.9 2 2 2h44c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2zM14.2 40.9H7.1V18h7.1v22.9zm-3.5-26c-2.3 0-4.1-1.8-4.1-4.1s1.8-4.1 4.1-4.1 4.1 1.8 4.1 4.1c0 2.2-1.8 4.1-4.1 4.1zm30.2 26h-7.1V29.8c0-2.7 0-6.1-3.7-6.1s-4.3 2.9-4.3 5.9v11.3h-7.1V18h6.8v3.1h.1c.9-1.8 3.3-3.7 6.7-3.7 7.2 0 8.5 4.7 8.5 10.9v12.6z"/></svg>
						</a>
					<?php endif; ?>
				</nav>
			<?php elseif ( is_page(2629) ||  in_array("2629", get_ancestors( get_the_ID(), 'page' )) ) : ?>
				<nav>
					<? if ( get_field('termination_support_services_facebook', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('termination_support_services_facebook', 'option'); ?>" title="Connect on Facebook">
							<svg viewBox="0 0 48 48"><path d="M19.33 46V25.93h-6.755v-7.822h6.754v-5.769C19.33 5.645 23.418 2 29.39 2c2.861 0 5.32.213 6.036.308v6.996l-4.142.002c-3.248 0-3.876 1.543-3.876 3.808v4.994h7.745l-1.008 7.822h-6.737V46h-8.078z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('termination_support_services_twitter', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('termination_support_services_twitter', 'option'); ?>" title="Connect on Twitter">
							<svg viewBox="0 0 48 48"><path d="M48 9.1c-1.8.8-3.7 1.3-5.7 1.6 2-1.2 3.6-3.1 4.3-5.4-1.9 1.1-4 1.9-6.3 2.4-1.8-1.9-4.4-3.1-7.2-3.1-5.4 0-9.8 4.4-9.8 9.8 0 .8.1 1.5.3 2.2-8.1-.4-15.4-4.3-20.3-10.3C2.5 7.8 2 9.4 2 11.2c0 3.4 1.7 6.4 4.4 8.2-1.6-.1-3.1-.5-4.5-1.2v.1c0 4.8 3.4 8.8 7.9 9.7-.8.2-1.7.3-2.6.3-.6 0-1.3-.1-1.9-.2 1.3 3.9 4.9 6.8 9.2 6.8-3.4 2.6-7.6 4.2-12.2 4.2-.8 0-1.6 0-2.3-.1 4.4 2.8 9.5 4.4 15.1 4.4 18.1 0 28-15 28-28v-1.3c1.9-1.3 3.6-3 4.9-5z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('termination_support_services_linkedin', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('termination_support_services_linkedin', 'option'); ?>" title="Connect on Linkedin">
							<svg viewBox="0 0 48 48"><path d="M46 0H2C.9 0 0 .9 0 2v44c0 1.1.9 2 2 2h44c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2zM14.2 40.9H7.1V18h7.1v22.9zm-3.5-26c-2.3 0-4.1-1.8-4.1-4.1s1.8-4.1 4.1-4.1 4.1 1.8 4.1 4.1c0 2.2-1.8 4.1-4.1 4.1zm30.2 26h-7.1V29.8c0-2.7 0-6.1-3.7-6.1s-4.3 2.9-4.3 5.9v11.3h-7.1V18h6.8v3.1h.1c.9-1.8 3.3-3.7 6.7-3.7 7.2 0 8.5 4.7 8.5 10.9v12.6z"/></svg>
						</a>
					<?php endif; ?>
				</nav>
			<?php elseif ( is_page(2642) || in_array("2642", get_ancestors( get_the_ID(), 'page' )) ) : ?>
				<nav>
					<? if ( get_field('crisis_facebook', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('crisis_facebook', 'option'); ?>" title="Connect on Facebook">
							<svg viewBox="0 0 48 48"><path d="M19.33 46V25.93h-6.755v-7.822h6.754v-5.769C19.33 5.645 23.418 2 29.39 2c2.861 0 5.32.213 6.036.308v6.996l-4.142.002c-3.248 0-3.876 1.543-3.876 3.808v4.994h7.745l-1.008 7.822h-6.737V46h-8.078z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('crisis_twitter', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('crisis_twitter', 'option'); ?>" title="Connect on Twitter">
							<svg viewBox="0 0 48 48"><path d="M48 9.1c-1.8.8-3.7 1.3-5.7 1.6 2-1.2 3.6-3.1 4.3-5.4-1.9 1.1-4 1.9-6.3 2.4-1.8-1.9-4.4-3.1-7.2-3.1-5.4 0-9.8 4.4-9.8 9.8 0 .8.1 1.5.3 2.2-8.1-.4-15.4-4.3-20.3-10.3C2.5 7.8 2 9.4 2 11.2c0 3.4 1.7 6.4 4.4 8.2-1.6-.1-3.1-.5-4.5-1.2v.1c0 4.8 3.4 8.8 7.9 9.7-.8.2-1.7.3-2.6.3-.6 0-1.3-.1-1.9-.2 1.3 3.9 4.9 6.8 9.2 6.8-3.4 2.6-7.6 4.2-12.2 4.2-.8 0-1.6 0-2.3-.1 4.4 2.8 9.5 4.4 15.1 4.4 18.1 0 28-15 28-28v-1.3c1.9-1.3 3.6-3 4.9-5z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('crisis_linkedin', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('crisis_linkedin', 'option'); ?>" title="Connect on Linkedin">
							<svg viewBox="0 0 48 48"><path d="M46 0H2C.9 0 0 .9 0 2v44c0 1.1.9 2 2 2h44c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2zM14.2 40.9H7.1V18h7.1v22.9zm-3.5-26c-2.3 0-4.1-1.8-4.1-4.1s1.8-4.1 4.1-4.1 4.1 1.8 4.1 4.1c0 2.2-1.8 4.1-4.1 4.1zm30.2 26h-7.1V29.8c0-2.7 0-6.1-3.7-6.1s-4.3 2.9-4.3 5.9v11.3h-7.1V18h6.8v3.1h.1c.9-1.8 3.3-3.7 6.7-3.7 7.2 0 8.5 4.7 8.5 10.9v12.6z"/></svg>
						</a>
					<?php endif; ?>
				</nav>
			<?php else : ?>
				<nav>
					<? if ( get_field('default_facebook', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('default_facebook', 'option'); ?>" title="Connect on Facebook">
							<svg viewBox="0 0 48 48"><path d="M19.33 46V25.93h-6.755v-7.822h6.754v-5.769C19.33 5.645 23.418 2 29.39 2c2.861 0 5.32.213 6.036.308v6.996l-4.142.002c-3.248 0-3.876 1.543-3.876 3.808v4.994h7.745l-1.008 7.822h-6.737V46h-8.078z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('default_twitter', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('default_twitter', 'option'); ?>" title="Connect on Twitter">
							<svg viewBox="0 0 48 48"><path d="M48 9.1c-1.8.8-3.7 1.3-5.7 1.6 2-1.2 3.6-3.1 4.3-5.4-1.9 1.1-4 1.9-6.3 2.4-1.8-1.9-4.4-3.1-7.2-3.1-5.4 0-9.8 4.4-9.8 9.8 0 .8.1 1.5.3 2.2-8.1-.4-15.4-4.3-20.3-10.3C2.5 7.8 2 9.4 2 11.2c0 3.4 1.7 6.4 4.4 8.2-1.6-.1-3.1-.5-4.5-1.2v.1c0 4.8 3.4 8.8 7.9 9.7-.8.2-1.7.3-2.6.3-.6 0-1.3-.1-1.9-.2 1.3 3.9 4.9 6.8 9.2 6.8-3.4 2.6-7.6 4.2-12.2 4.2-.8 0-1.6 0-2.3-.1 4.4 2.8 9.5 4.4 15.1 4.4 18.1 0 28-15 28-28v-1.3c1.9-1.3 3.6-3 4.9-5z"/></svg>
						</a>
					<?php endif; ?>
					<?php if ( get_field('default_linkedin', 'option') ) : ?>
						<a target="_blank" href="<?php the_field('default_linkedin', 'option'); ?>" title="Connect on Linkedin">
							<svg viewBox="0 0 48 48"><path d="M46 0H2C.9 0 0 .9 0 2v44c0 1.1.9 2 2 2h44c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2zM14.2 40.9H7.1V18h7.1v22.9zm-3.5-26c-2.3 0-4.1-1.8-4.1-4.1s1.8-4.1 4.1-4.1 4.1 1.8 4.1 4.1c0 2.2-1.8 4.1-4.1 4.1zm30.2 26h-7.1V29.8c0-2.7 0-6.1-3.7-6.1s-4.3 2.9-4.3 5.9v11.3h-7.1V18h6.8v3.1h.1c.9-1.8 3.3-3.7 6.7-3.7 7.2 0 8.5 4.7 8.5 10.9v12.6z"/></svg>
						</a>
					<?php endif; ?>
				</nav>
			<?php endif; ?>
		</section>
	</div>
</footer>

<button class="back-to-top is-blue">
	<svg>
		<use xlink:href="#arrow-up" />
	</svg>
</button>