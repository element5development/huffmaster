<?php 
/*----------------------------------------------------------------*\

	NEWS & EVENTS FEED

\*----------------------------------------------------------------*/
?>

<section id="team" class="team">
	<h2><?php the_sub_field('headline') ?></h2>
	<div>
		<?php $post_objects = get_sub_field('members'); ?>
		<?php if( $post_objects ): ?>
				<?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
						<?php setup_postdata($post); ?>
						<article>
							<?php $headshot = get_field('headshot'); ?> 
							<img src="<?php echo $headshot['sizes']['small']; ?>" alt="<?php echo $headshot['alt']; ?>" />
							<h3>
								<?php 
									if ( get_field('post_title') ) :
										the_field('post_title');
									else :
										the_title();
									endif;
								?>
							</h3>
							<?php if ( get_field('title_description') ) : ?>
								<p>
									<?php the_field('title_description'); ?>
								</p>
							<?php endif; ?>
							<a href="<?php the_permalink(); ?>"></a>
						</article>
				<?php endforeach; ?>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>
	</div>
</section>