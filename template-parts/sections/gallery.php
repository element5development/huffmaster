<?php 
/*----------------------------------------------------------------*\

	IMAGE GALLERY WITH LIGHTBOX ZOOM

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="gallery">
	<?php if ( get_sub_field('headline') ) : ?>
		<h2><?php the_sub_field('headline'); ?></h2>
	<?php endif; ?>
	<div class="gallery-items">
		<?php $images = get_sub_field('gallery'); ?>
		<?php foreach( $images as $image ): ?>
			<a class="gallery-item" href="<?php echo $image['url']; ?>">
				<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		<?php endforeach; ?>
	</div>
</section>