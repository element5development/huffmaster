<?php 
/*----------------------------------------------------------------*\

	HEADER WITH IMAGE BACKGROUND

\*----------------------------------------------------------------*/
?>

	<?php if ( get_field('slider') ) : ?>
	<div class="header-wrap">
		<header class="page-title is-slider <?php if ( get_field('video') ) : ?>has-video<?php endif; ?>">
			<?php while ( have_rows('slides') ) : the_row(); ?>
				<?php $bg = get_sub_field('slide_background'); ?>
				<div class="title-slide" style="background-image: url('<?php echo $bg['sizes']['xlarge']; ?>');">
					<section>
						<h1><?php the_sub_field('slide_title'); ?></h1>
						<?php if ( get_sub_field('slide_description') ) : ?>
							<p>
								<?php the_sub_field('slide_description'); ?>
							</p>
						<?php endif; ?>
						<?php if ( get_sub_field('slide_button') ) : ?>
							<?php $button = get_sub_field('slide_button'); ?>
							<a class="button" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
								<?php echo $button['title']; ?>
							</a>
						<?php endif; ?>
					</section>
					<div class="overlay"></div>
				</div>
			<?php endwhile; ?>
		</header>
		<?php if ( get_field('video') ) : ?>
		<div>
			<iframe src="<?php the_field('video_url'); ?>" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
		</div>
		<?php endif; ?>
	</div>
	<?php else : ?>
	<div class="header-wrap">
		<?php 
			if ( get_field('title_background') ) :
				$background = get_field('title_background'); 
				$bg = $background['sizes']['xlarge'];
			elseif ( is_archive() || is_home() ) :
				$bg = get_template_directory_uri() . '/dist/images/archive_bg.jpg';
			else : 
				$bg = get_template_directory_uri() . '/dist/images/page_bg.png';
			endif;
		?>
		<header class="page-title <?php if ( get_field('video') ) : ?>has-video<?php endif; ?>" style="background-image: url('<?php echo $bg; ?>');">
			<section>
				<h1>
					<?php 
						if ( is_archive() || is_home() ) :
							echo 'News & Events';
						elseif ( is_search() ) :
							echo 'search results for ' . get_search_query();
						elseif ( get_field('post_title') ) :
							the_field('post_title');
						else :
							the_title();
						endif;
					?>
				</h1>
				<?php if ( !is_archive() && !is_home() && get_field('news_or_event') == 'Event' ) : ?>
					<p class="date"><?php the_field('date'); ?> @ <?php the_field('location'); ?></p>
				<?php elseif ( get_field('title_description') ) : ?>
					<p>
						<?php the_field('title_description'); ?>
					</p>
				<?php endif; ?>
				<?php if ( get_field('title_button') ) : ?>
					<?php $button = get_field('title_button'); ?>
					<a class="button" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
						<?php echo $button['title']; ?>
					</a>
				<?php endif; ?>
			</section>
			<div class="overlay"></div>
		</header>
		<?php if ( get_field('video') ) : ?>
		<div>
			<iframe src="<?php the_field('video_url'); ?>" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
		</div>
		<?php endif; ?>
	</div>
	<?php endif; ?>