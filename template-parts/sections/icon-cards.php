<?php 
/*----------------------------------------------------------------*\

	ICON CARD GRID

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="icon-cards">
	<h2><?php the_sub_field('headline'); ?></h2>
	<?php if( have_rows('cards') ): ?>
		<div class="cards">
			<?php while ( have_rows('cards') ) : the_row(); ?>

				<div class="card">
					<?php $icon = get_sub_field('icon'); ?>
					<div class="icon">
						<img src="<?php echo $icon['sizes']['small']; ?>" alt="<?php echo $icon['alt']; ?>" />
					</div>
					<h3><?php the_sub_field('title'); ?></h3>
					<p><?php the_sub_field('description'); ?></p>
				</div>

			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>