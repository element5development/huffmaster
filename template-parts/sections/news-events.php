<?php 
/*----------------------------------------------------------------*\

	NEWS & EVENTS FEED

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="news-events">
	<h2>News & Events</h2>
	<?php $term = get_sub_field('category'); ?>
	<a href="<?php echo get_term_link( $term ); ?>">
		View All
		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/arrow-right.svg" />
	</a>
	<?php 
	$args = array(
		'posts_per_page' => 3,
		'order' => 'DESC',
		'category_name' => $term->name,
	);
	$news_events = new WP_Query( $args );
	?>

	<?php if ( $news_events->have_posts() ) : ?>
		<div>
			<?php while ( $news_events->have_posts() ) : $news_events->the_post(); ?>
				<?php get_template_part('template-parts/elements/preview-blog'); ?>
			<?php endwhile; ?>
		</div>
	<?php endif; wp_reset_postdata(); ?>	
</section>