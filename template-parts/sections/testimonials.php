<?php 
/*----------------------------------------------------------------*\

	TESTIMONIALS SLIDER

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="testimonials-block">
	<div>
		<h2><?php the_sub_field('headline'); ?></h2>
		<div class="testimonials">
			<?php while ( have_rows('testimonies') ) : the_row(); ?>
				<div class="testimony">
				<?php if ( get_sub_field('quote') ) : ?>
					<p><?php the_sub_field('quote'); ?></p>
					<hr>
				<?php endif; ?>
					<p><strong><?php the_sub_field('quotee'); ?></strong></p>
					<p><?php the_sub_field('company'); ?></p>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>