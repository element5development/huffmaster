<?php 
/*----------------------------------------------------------------*\

	LOGO CARDS

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="logo-cards">
	<h2><?php the_sub_field('headline'); ?></h2>
	<?php if( have_rows('cards') ): ?>
		<div class="cards">
			<?php while ( have_rows('cards') ) : the_row(); ?>

				<div class="card">
					<?php $logo = get_sub_field('logo'); ?>
					<div class="logo">
						<img src="<?php echo $logo['sizes']['small']; ?>" alt="<?php echo $logo['alt']; ?>" />
					</div>
					<h3><?php the_sub_field('headline'); ?></h3>
					<p><?php the_sub_field('description'); ?></p>
				</div>

			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>