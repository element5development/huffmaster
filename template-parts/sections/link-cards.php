<?php 
/*----------------------------------------------------------------*\

	LINKS GRID

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="link-cards">
	<h2><?php the_sub_field('headline'); ?></h2>
	<?php if( have_rows('cards') ): ?>
		<div class="cards">
			<?php while ( have_rows('cards') ) : the_row(); ?>

				<div class="card">
					<?php $link = get_sub_field('link'); ?>
					<?php $icon = get_sub_field('icon'); ?>
					<div class="icon">
						<img src="<?php echo $icon['sizes']['small']; ?>" alt="<?php echo $icon['alt']; ?>" />
					</div>
					<h3><?php echo $link['title']; ?></h3>
					<p><?php the_sub_field('description'); ?></p>
					<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"></a>
				</div>

			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>