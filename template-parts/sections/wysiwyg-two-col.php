<?php 
/*----------------------------------------------------------------*\

	TWO COLUMN WYSIWYG EDITOR

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="wysiwyg-block has-two-col">
	<?php if ( get_sub_field('single_headline') ) : ?>
		<h2><?php the_sub_field('single_headline'); ?></h2>
	<?php endif; ?>
	<div>
		<?php the_sub_field('wysiwyg_left'); ?>
	</div>
	<div>
	<?php the_sub_field('wysiwyg_right'); ?>
	</div>
</section>