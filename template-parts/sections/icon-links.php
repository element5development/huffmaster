<?php 
/*----------------------------------------------------------------*\

	RESOURCES

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="icon-links">
	<h2><?php the_sub_field('headline'); ?></h2>
	<?php if( have_rows('links') ): ?>
		<div class="links">
			<?php while ( have_rows('links') ) : the_row(); ?>

				<div class="link">
					<?php $link = get_sub_field('link'); ?>
					<?php $icon = get_sub_field('icon'); ?>
					<img src="<?php echo $icon['sizes']['small']; ?>" alt="<?php echo $icon['alt']; ?>" />
					<h3><?php echo $link['title']; ?></h3>
					<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"></a>
				</div>

			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>