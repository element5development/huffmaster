<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>
<div class="navigation-block">
	<!-- logo -->
	<?php if ( is_page(401) || in_array("401", get_ancestors( get_the_ID(), 'page' )) || is_page(398) ) : ?>
		<a class="home" href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-home.svg" alt="back home" />
		</a>
		<a class="logo" href="<?php the_permalink(401); ?>">
			<?php $image= get_field('healthcare_strikes_logo', 'option'); ?>
			<img src="<?php echo $image['sizes']['small']; ?>" alt="Huffmaster Healthcare Strikes" />
		</a>
	<?php elseif ( is_page(428) ||  in_array("428", get_ancestors( get_the_ID(), 'page' )) || is_page(399) ) : ?>
		<a class="home" href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-home.svg" alt="back home" />
		</a>
		<a class="logo" href="<?php the_permalink(428); ?>">
			<?php $image= get_field('industrial_staffing_logo', 'option'); ?>
			<img src="<?php echo $image['sizes']['small']; ?>" alt="Huffmaster Industrial Staffing" />
		</a>
	<?php elseif ( is_page(434) ||  in_array("434", get_ancestors( get_the_ID(), 'page' )) || in_array("400", get_ancestors( get_the_ID(), 'page' ))  ) : ?>
		<a class="home" href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-home.svg" alt="back home" />
		</a>
		<a class="logo" href="<?php the_permalink(434); ?>">
			<?php $image= get_field('security_services_logo', 'option'); ?>
			<img src="<?php echo $image['sizes']['small']; ?>" alt="Huffmaster Security Services" />
		</a>
	<?php elseif ( is_page(422) ||  in_array("422", get_ancestors( get_the_ID(), 'page' )) || is_page(400) ) : ?>
		<a class="home" href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-home.svg" alt="back home" />
		</a>
		<a class="logo" href="<?php the_permalink(422); ?>">
			<?php $image= get_field('strike_services_logo', 'option'); ?>
			<img src="<?php echo $image['sizes']['small']; ?>" alt="Huffmaster Striker Services" />
		</a>
	<?php elseif ( is_page(2629) ||  in_array("2629", get_ancestors( get_the_ID(), 'page' )) ) : ?>
		<a class="home" href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-home.svg" alt="back home" />
		</a>
		<a class="logo" href="<?php the_permalink(2629); ?>">
			<?php $image= get_field('termination_support_services_logo', 'option'); ?>
			<img src="<?php echo $image['sizes']['small']; ?>" alt="Huffmaster Termination Support Services" />
		</a>
	<?php elseif ( is_page(2642) || in_array("2642", get_ancestors( get_the_ID(), 'page' )) ) : ?>
		<a class="home" href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-home.svg" alt="back home" />
		</a>
		<a class="logo" href="<?php the_permalink(2642); ?>">
			<?php $image= get_field('crisis_logo', 'option'); ?>
			<img src="<?php echo $image['sizes']['small']; ?>" alt="Huffmaster Healthcare Strikes" />
		</a>
	<?php else : ?>
		<a class="logo" href="<?php echo get_home_url(); ?>">
			<?php $image= get_field('default_logo', 'option'); ?>
			<img src="<?php echo $image['sizes']['small']; ?>" alt="Huffmaster" />
		</a>
	<?php endif; ?>
	<!-- phone -->
	<?php if ( is_page(398) ) : ?>
		<?php $phone_number = preg_replace( '/[^0-9]/', '', get_field('healthcare_strikes_phone', 'option') ); ?>
		<a class="phone button is-blue" href="tel:+1<?php echo $phone_number; ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon_phone.svg" alt="call our office" />
			<span>+1 <?php the_field('healthcare_strikes_phone', 'option'); ?></span>
		</a>
	<?php elseif ( is_page(399) ) : ?>
		<?php $phone_number = preg_replace( '/[^0-9]/', '', get_field('industrial_staffing_phone', 'option') ); ?>
		<a class="phone button is-blue" href="tel:+1<?php echo $phone_number; ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon_phone.svg" alt="call our office" />
			<span>+1 <?php the_field('industrial_staffing_phone', 'option'); ?></span>
		</a>
	<?php elseif ( in_array("400", get_ancestors( get_the_ID(), 'page' ))  ) : ?>
		<?php $phone_number = preg_replace( '/[^0-9]/', '', get_field('security_services_phone', 'option') ); ?>
		<a class="phone button is-blue" href="tel:+1<?php echo $phone_number; ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon_phone.svg" alt="call our office" />
			<span>+1 <?php the_field('security_services_phone', 'option'); ?></span>
		</a>
	<?php elseif ( is_page(400) ) : ?>
		<?php $phone_number = preg_replace( '/[^0-9]/', '', get_field('strike_services_phone', 'option') ); ?>
		<a class="phone button is-blue" href="tel:+1<?php echo $phone_number; ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon_phone.svg" alt="call our office" />
			<span>+1 <?php the_field('strike_services_phone', 'option'); ?></span>
		</a>
	<?php elseif ( is_page(2629) ) : ?>
		<?php $phone_number = preg_replace( '/[^0-9]/', '', get_field('termination_support_services_phone', 'option') ); ?>
		<a class="phone button is-blue" href="tel:+1<?php echo $phone_number; ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon_phone.svg" alt="call our office" />
			<span>+1 <?php the_field('termination_support_services_phone', 'option'); ?></span>
		</a>
	<?php else : ?>
		<?php $phone_number = preg_replace( '/[^0-9]/', '', get_field('default_phone', 'option') ); ?>
		<a class="phone button is-blue" href="tel:+1<?php echo $phone_number; ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon_phone.svg" alt="call our office" />
			<span>+1 <?php the_field('default_phone', 'option'); ?></span>
		</a>
	<?php endif; ?>
	<!-- menu -->
	<form role="search" method="get" action="<?php echo get_site_url(); ?>">
		<input type="search" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
		<button type="submit" class="is-blue">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-search-selected.svg" alt="search our site" />
		</button>
	</form>
	<button class="search-button">
		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-search.svg" alt="search our site" />
	</button>
	<button class="menu-button">
		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/icon-menu.svg" alt="open navigation" />
	</button>
	<nav>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_nav' )); ?>
	</nav>
	<?php if (get_field('notification', 'option')) : ?>
		<div class="notification">
			<a href="<?php the_field('link', 'option'); ?>"><?php the_field('notification', 'option'); ?></a>
			<button>Hide</button>
		</div>
	<?php endif; ?>
</div>