<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-search">
	<div class="icon">
		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/search-result.svg" />
	</div>
	<h3>
		<?php 
			if ( get_field('post_title') ) :
				the_field('post_title');
			else :
				the_title();
			endif;
		?>
	</h3>
	<p><?php echo get_excerpt(150); ?></p>
	<a href="<?php the_permalink(); ?>"></a>
</article>
