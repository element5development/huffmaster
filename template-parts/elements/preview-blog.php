<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-blog">			
	<?php if ( get_field('news_or_event') == 'Event' ) : ?>
		<div class="icon event">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/events.svg" alt="event" />
		</div>
	<?php else : ?>
		<div class="icon">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/news.svg" alt="post" />
		</div>
	<?php endif; ?>
	<h3>
		<?php 
			if ( get_field('post_title') ) :
				the_field('post_title');
			else :
				the_title();
			endif;
		?>
	</h3>
	<?php if ( get_field('date') ) : ?>
		<p class="date"><?php the_field('date'); ?></p>
	<?php endif; ?>
	<p><?php echo get_excerpt(150); ?></p>
	<a href="<?php the_permalink(); ?>"></a>
</article>
