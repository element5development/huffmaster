<?php

/*----------------------------------------------------------------*\

	CUSTOM POST TYPES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Post Type Key: team
function create_team_cpt() {
	$labels = array(
		'name' => __( 'Team', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Team', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Team', 'textdomain' ),
		'name_admin_bar' => __( 'Team', 'textdomain' ),
		'archives' => __( 'Team Archives', 'textdomain' ),
		'attributes' => __( 'Team Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Team:', 'textdomain' ),
		'all_items' => __( 'All Team', 'textdomain' ),
		'add_new_item' => __( 'Add New Team', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Team', 'textdomain' ),
		'edit_item' => __( 'Edit Team', 'textdomain' ),
		'update_item' => __( 'Update Team', 'textdomain' ),
		'view_item' => __( 'View Team', 'textdomain' ),
		'view_items' => __( 'View Team', 'textdomain' ),
		'search_items' => __( 'Search Team', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Team', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Team', 'textdomain' ),
		'items_list' => __( 'Team list', 'textdomain' ),
		'items_list_navigation' => __( 'Team list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Team list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Team', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-groups',
		'supports' => array('title', 'excerpt', 'page-attributes', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'rewrite' => array( 'slug' => 'about' ),
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'team', $args );
}
add_action( 'init', 'create_team_cpt', 0 );
// BREADCRUMB CORRECTION
function breadcrumb_links( $links ) {
if ( is_single() ) {
	$cpt_object = get_post_type_object( get_post_type() );
	if ( ! $cpt_object->_builtin ) {
		$landing_page = get_page_by_path( $cpt_object->rewrite['slug'] );
		$oldest_parent = $landing_page;
		$safty = 0; //remove the safety at your own risk :)
		while($oldest_parent->post_parent != 0){
			$oldest_parent = get_post($oldest_parent->post_parent);
			array_splice( $links, -1, 0, array(
				array(
					'id'    => $oldest_parent->ID
				)
			));
			if($safty++ > 5) break;
		}

		array_splice( $links, -1, 0, array(
			array(
				'id'    => $landing_page->ID
			)
		));
	}
}
return $links;
}
add_filter( 'wpseo_breadcrumb_links', 'breadcrumb_links' );
