<?php
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Notification Banner',
		'menu_title'	=> 'Notification',
		'menu_slug' 	=> 'notification-banner',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));	
	acf_add_options_page(array(
		'page_title' 	=> 'Department Information',
		'menu_title'	=> 'Departments',
		'menu_slug' 	=> 'department-info',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));	
}
?>