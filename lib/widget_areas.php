<?php

/*----------------------------------------------------------------*\

	CUSTOM WIDGET AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/

function first_footer_sidebar() {
	$args = array(
		'name'          => __( 'First Footer Area' ),
		'id'            => 'first-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'first_footer_sidebar' );

function second_footer_sidebar() {
	$args = array(
		'name'          => __( 'Second Footer Area' ),
		'id'            => 'second-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'second_footer_sidebar' );

function third_footer_sidebar() {
	$args = array(
		'name'          => __( 'Third Footer Area' ),
		'id'            => 'third-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'third_footer_sidebar' );

function fourth_footer_sidebar() {
	$args = array(
		'name'          => __( 'Fourth Footer Area' ),
		'id'            => 'fourth-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'fourth_footer_sidebar' );

function fifth_footer_sidebar() {
	$args = array(
		'name'          => __( 'Fifth Footer Area' ),
		'id'            => 'fifth-footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'fifth_footer_sidebar' );