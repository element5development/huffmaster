<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://ahsanrehman.com
 * @since      1.0.0
 *
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

	<h1>Bullhorn to Wordpress Settings</h1>

	<form method="post" action="options.php">
        
        <?php
          settings_fields( 'bh2wp-settings' );
          do_settings_sections( 'bh2wp-settings' );
        ?>

		<table class="form-table">
		    <tbody>
		        <tr>
		            <th scope="row">
		                <label for="bh2wp_client_id">Client ID</label>
		            </th>
		            <td>
		                <input name="bh2wp_client_id" type="text" id="bh2wp_client_id" value="<?php echo esc_attr( get_option('bh2wp_client_id') ); ?>" class="regular-text" required="required">
		                <p class="description">Your bullhorn client id.</p>
		            </td>
		        </tr>

		        <tr>
		            <th scope="row">
		                <label for="bh2wp_client_secret">Client Secret</label>
		            </th>
		            <td>
		                <input name="bh2wp_client_secret" type="text" id="bh2wp_client_secret" value="<?php echo esc_attr( get_option('bh2wp_client_secret') ); ?>" class="regular-text" required="required">
		                <p class="description">Your bullhorn client secret key.</p>
		            </td>
		        </tr>

		        <tr>
		            <th scope="row">
		                <label for="bh2wp_api_username">API Username</label>
		            </th>
		            <td>
		                <input name="bh2wp_api_username" type="text" id="bh2wp_api_username" value="<?php echo esc_attr( get_option('bh2wp_api_username') ); ?>" class="regular-text" required="required">
		                <p class="description">Your bullhorn client api username.</p>
		            </td>
		        </tr>

		        <tr>
		            <th scope="row">
		                <label for="bh2wp_api_password">API Password</label>
		            </th>
		            <td>
		                <input name="bh2wp_api_password" type="password" id="bh2wp_api_password" value="<?php echo esc_attr( get_option('bh2wp_api_password') ); ?>" class="regular-text" required="required">
		                <p class="description">Your bullhorn client api password.</p>
		            </td>
		        </tr>

		    </tbody>
		</table>

		<?php submit_button(); ?>
	</form>
	<br>
	<div class="usage_information">
		<h3>Shortcode:</h3>
		Default: <strong>[bh2wp]</strong> <br>
		With Industry: <strong>[bh2wp industries="1100060"]</strong> (use comma separated values for multiple) <br>
		With Categories: <strong>[bh2wp categories="2000000"]</strong> (use comma separated values for multiple) <br>
		With Style Color: <strong>[bh2wp color="#000"]</strong> <br>
		With logo: <strong>[bh2wp logo="full path to image"]</strong> <br>
	</div>
	<!-- 
	1100060 => Industrial Strikes
	1100058 => Healthcare Strikes
	1100062 => Crisis Security
	1100059 => Healthcare Travel
	-->

</div>