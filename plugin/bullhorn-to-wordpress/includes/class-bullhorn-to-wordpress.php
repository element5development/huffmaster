<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://ahsanrehman.com
 * @since      1.0.0
 *
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/includes
 * @author     Ahsan Rehman <ahsan-rehman@live.com>
 */
class Bullhorn_To_Wordpress {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Bullhorn_To_Wordpress_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'bullhorn-to-wordpress';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Bullhorn_To_Wordpress_Loader. Orchestrates the hooks of the plugin.
	 * - Bullhorn_To_Wordpress_i18n. Defines internationalization functionality.
	 * - Bullhorn_To_Wordpress_Admin. Defines all hooks for the admin area.
	 * - Bullhorn_To_Wordpress_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bullhorn-to-wordpress-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bullhorn-to-wordpress-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-bullhorn-to-wordpress-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-bullhorn-to-wordpress-public.php';

		/**
		 * The class responsible for bullhorn integrations
		 */
		// require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bullhorn-to-wordpress-interface.php';
		// require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/vendor/autoload.php';

		$this->loader = new Bullhorn_To_Wordpress_Loader();
		// $this->bh2wp = new Bullhorn_To_Wordpress_Api();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Bullhorn_To_Wordpress_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Bullhorn_To_Wordpress_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Bullhorn_To_Wordpress_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'create_settings_page');
		$this->loader->add_action( 'admin_init', $plugin_admin, 'settings_page_option');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Bullhorn_To_Wordpress_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_ajax_submit_application_form', $plugin_public, 'submit_application_form' );
		$this->loader->add_action( 'wp_ajax_nopriv_submit_application_form', $plugin_public, 'submit_application_form' );
		$this->loader->add_action( 'wp_ajax_candidate_check_form', $plugin_public, 'candidate_check_form' );
		$this->loader->add_action( 'wp_ajax_nopriv_candidate_check_form', $plugin_public, 'candidate_check_form' );
		$this->loader->add_action( 'wp_ajax_save_job_alerts', $plugin_public, 'save_job_alerts' );
		$this->loader->add_action( 'wp_ajax_nopriv_save_job_alerts', $plugin_public, 'save_job_alerts' );
		$this->loader->add_action( 'wp_footer', $plugin_public, 'view_application_form' );
		$this->loader->add_shortcode( 'bh2wp', $plugin_public, 'view_shortcode' );
		$this->loader->add_shortcode( 'bh2wp_unsubscribe', $plugin_public, 'unsubscribe_shortcode' );
		$this->loader->add_action( 'my_cronjob_action', $plugin_public, 'send_job_alerts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Bullhorn_To_Wordpress_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
