<?php

/**
 * Fired during plugin activation
 *
 * @link       http://ahsanrehman.com
 * @since      1.0.0
 *
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/includes
 * @author     Ahsan Rehman <ahsan-rehman@live.com>
 */
class Bullhorn_To_Wordpress_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;

// 		if (is_multisite()) {
// 			$current_site = get_current_site();
// 			$table_name = $wpdb->prefix . $current_site->id . '_job_alerts';
// 		} else {
			$table_name = $wpdb->prefix . 'job_alerts';
// 		}

		if ($wpdb->get_var('SHOW TABLES LIKE '.$table_name) != $table_name) {
			
			$charset_collate = $wpdb->get_charset_collate();
			$sql = "CREATE TABLE $table_name (
				id int(11) NOT NULL AUTO_INCREMENT,
				subscriber_name varchar(255) DEFAULT '' NOT NULL,
				subscriber_email varchar(255) DEFAULT '' NOT NULL,
				search_keyword varchar(255) DEFAULT '' NOT NULL,
				search_location varchar(255) DEFAULT '' NOT NULL,
				random_id varchar(255) DEFAULT '' NOT NULL,
				created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
				updated_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
				PRIMARY KEY  (id)
			) $charset_collate;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
	}

}
