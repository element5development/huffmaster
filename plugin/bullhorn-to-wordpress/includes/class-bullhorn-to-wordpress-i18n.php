<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://ahsanrehman.com
 * @since      1.0.0
 *
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/includes
 * @author     Ahsan Rehman <ahsan-rehman@live.com>
 */
class Bullhorn_To_Wordpress_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'bullhorn-to-wordpress',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
