<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://ahsanrehman.com
 * @since      1.0.0
 *
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/includes
 * @author     Ahsan Rehman <ahsan-rehman@live.com>
 */
class Bullhorn_To_Wordpress_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
