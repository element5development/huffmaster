<?php

require_once WP_PLUGIN_DIR.'/bullhorn-to-wordpress/includes/vendor/autoload.php';

use jonathanraftery\Bullhorn\Rest\Client as BullhornClient;

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://ahsanrehman.com
 * @since      1.0.0
 *
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/public
 * @author     Ahsan Rehman <ahsan-rehman@live.com>
 */
class Bullhorn_To_Wordpress_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bullhorn_To_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bullhorn_To_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
		wp_register_style( 'datatables-css', '//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' );
		wp_enqueue_style('datatables-css');

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bullhorn-to-wordpress-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Bullhorn_To_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Bullhorn_To_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */


		wp_register_script( 'datatables-js', '//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js', null, $this->version, true );
		wp_enqueue_script('datatables-js');

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bullhorn-to-wordpress-public.js', array( 'jquery' ), $this->version, true );

		wp_localize_script( $this->plugin_name, 'settings', array(
		    'ajaxurl'    => admin_url( 'admin-ajax.php' ),
		) );

	}

	public function authentication($client_id, $client_secret, $username, $password)	{

		$client = '';
		while (true) {
			try {
				
				$client = new BullhornClient(
				    $client_id,
				    $client_secret
				);
				
				$client->initiateSession(
				    $username,
				    $password
				);

				break;
			} catch (Exception $e) {
				if ($e->getCode() != 500) {
					$client = 'Something wrong with bullhorn connection please make sure credentials are correct.';
					$client .= 'Error Code: ' .$e->getCode();
					$client .= '. Error Message: ' .$e->getMessage();
					break;
				}
			}
			usleep(200000);
		}

		return $client;

	}

	public static function view_shortcode( $atts ) {

		$bhOptions = shortcode_atts( array(
	        'logo' => '',
	        'categories' => '',
	        'industries' => '',
	        'color' => '#000',
	        'show_cat' => 'false',
	        'count' => '',
	    ), $atts );

	    $GLOBALS['shortcode_values'] = $bhOptions;

	    // echo "<pre>";
	    // self::send_job_alerts();
	    // die;

		$jobs = self::get_all_jobs($bhOptions['industries'], $bhOptions['categories'], $bhOptions['count']);
		$locations = self::get_all_locations($bhOptions['industries'], $bhOptions['categories'], $bhOptions['count']);
		
		ob_start();

		include plugin_dir_path(  __FILE__ ) . 'partials/bullhorn-to-wordpress-public-display.php';

		$content = ob_get_clean();
		return $content;
	}

	public function get_all_locations($industry=null, $category=null, $counts=null) {
		$industries = '';
		$categories = '';

		if ($counts) {
			$counts = '&count='.$counts;
		} else {
			$counts = '&count=999';
		}
		
		if ($industry) {
			if (strpos($industry, ',') !== false) {
				$industries = ' AND (';
				$arrayInd = explode(',', $industry);
				foreach ($arrayInd as $key => $value) {
					$industries .= 'businessSectors.id:'.$value.' OR ';
				}
				$industries .= ') ';
				$industries = str_replace(' OR )', ')', $industries);
			} else {
				$industries = ' AND businessSectors.id:'.$industry;
			}
		}

		if ($category) {
			if (strpos($category, ',') !== false) {
				$categories = ' AND (';
				$arrayCat = explode(',', $category);
				foreach ($arrayCat as $key => $value) {
					$categories .= 'categories.id:'.$value.' OR ';
				}
				$categories .= ') ';
				$categories = str_replace(' OR )', ')', $categories);
			} else {
				$categories = ' AND categories.id:'.$category;
			}
		}

		$response = '';
		while (true) {
			try {
				$client = self::authentication(
					get_option('bh2wp_client_id'),
					get_option('bh2wp_client_secret'),
					get_option('bh2wp_api_username'),
					get_option('bh2wp_api_password')
				);

				$response = $client->request(
				    'GET',
				    'search/JobOrder',
				    ['query' => 'query=isOpen:1 AND isDeleted:0 AND employmentType:"Temporary Strike Assignment" AND status:"Accepting Candidates"'.$industries.$categories.'&fields=address'.$counts.'&start=0&sort=-dateLastModified']
				);

				break;
			} catch (Exception $e) {
				if ($e->getCode() != 500) {
					$response = 'Something wrong with bullhorn connection please make sure credentials are correct.';
					$response .= 'Error Code: ' .$e->getCode();
					$response .= '. Error Message: ' .$e->getMessage();
					break;
				}
			}
			usleep(200000);
		}
		
		$resLocations = [];
	
		if (isset($response->data)) {
			foreach ($response->data as $key => $location) {
				if (isset($location->address->state) && $location->address->state != '') {
					$resLocations[$key] = $location->address->state;
				}
			}

			asort($resLocations);

		}

		return $resLocations;
	}

	public function get_all_jobs($industry=null, $category=null, $counts=null) {
		$industries = '';
		$categories = '';

		if ($counts) {
			$counts = '&count='.$counts;
		} else {
			$counts = '&count=999';
		}
		
		if ($industry) {
			if (strpos($industry, ',') !== false) {
				$industries = ' AND (';
				$arrayInd = explode(',', $industry);
				foreach ($arrayInd as $key => $value) {
					$industries .= 'businessSectors.id:'.$value.' OR ';
				}
				$industries .= ') ';
				$industries = str_replace(' OR )', ')', $industries);
			} else {
				$industries = ' AND businessSectors.id:'.$industry;
			}
		}

		if ($category) {
			if (strpos($category, ',') !== false) {
				$categories = ' AND (';
				$arrayCat = explode(',', $category);
				foreach ($arrayCat as $key => $value) {
					$categories .= 'categories.id:'.$value.' OR ';
				}
				$categories .= ') ';
				$categories = str_replace(' OR )', ')', $categories);
			} else {
				$categories = ' AND categories.id:'.$category;
			}
		}

		$response = '';
		while (true) {
			try {
				$client = self::authentication(
					get_option('bh2wp_client_id'),
					get_option('bh2wp_client_secret'),
					get_option('bh2wp_api_username'),
					get_option('bh2wp_api_password')
				);

				$response = $client->request(
				    'GET',
				    'search/JobOrder',
				    ['query' => 'query=isOpen:1 AND isDeleted:0 AND employmentType:"Temporary Strike Assignment" AND status:"Accepting Candidates"'.$industries.$categories.'&fields=id,title,description,customTextBlock1,address,correlatedCustomFloat3,dateLastModified,skills,specialties,categories'.$counts.'&start=0&sort=-dateLastModified']);

				break;
			} catch (Exception $e) {
				if ($e->getCode() != 500) {
					$response = 'Something wrong with bullhorn connection please make sure credentials are correct.';
					$response .= 'Error Code: ' .$e->getCode();
					$response .= '. Error Message: ' .$e->getMessage();
					break;
				}
			}
			usleep(200000);
		}

		return $response;
	}

	public function create_job_submission($candidate, $job_id) {
		$jobSubmission = [];
		$jobSubmission['status'] = 'New Lead';
		$jobSubmission['dateWebResponse'] = strtotime(date('Y-m-d h:i:s'));
		$jobSubmission['candidate'] = [
			'id' => $candidate
		];
		$jobSubmission['jobOrder'] = [
			'id' => $job_id
		];
		$jobSubmission = ['json' => $jobSubmission];

		$jobRes = $client->request(
			'PUT',
		    'entity/JobSubmission',
		    $jobSubmission
		);
	}

	public function search_candidate($email) {
		$response = '';
		while (true) {
			try {
				$client = self::authentication(
					get_option('bh2wp_client_id'),
					get_option('bh2wp_client_secret'),
					get_option('bh2wp_api_username'),
					get_option('bh2wp_api_password')
				);

				$response = $client->request(
				    'GET',
				    'search/Candidate',
				    ['query' => 'query=isDeleted:0 AND email:'.$email.'&fields=id']
				);

				break;
			} catch (Exception $e) {
				if ($e->getCode() != 500) {
					$response = 'Something wrong with bullhorn connection please make sure credentials are correct.';
					$response .= 'Error Code: ' .$e->getCode();
					$response .= '. Error Message: ' .$e->getMessage();
					break;
				}
			}
			usleep(200000);
		}

		if (isset($response->data[0]->id)) {
			return $response->data[0]->id;
		}

		return '';
	}

	public function create_candidate($candidate) {
		@ini_set('max_execution_time', 60);

		$candidateID = self::search_candidate($candidate['email']);

		$job_id = $candidate['job_id'];
		$resume = $candidate['resume'];

		print_r(count($resume));

		unset($candidate['job_id']);
		unset($candidate['resume']);
		$candidate = ['json' => $candidate];

		$response = '';
		while (true) {
			try {
				$client = self::authentication(
					get_option('bh2wp_client_id'),
					get_option('bh2wp_client_secret'),
					get_option('bh2wp_api_username'),
					get_option('bh2wp_api_password')
				);

				if ($candidateID == '') {
					$candidateRes = $client->request(
					    'PUT',
					    'entity/Candidate',
					    $candidate
					);
					$candidateID = $candidateRes->changedEntityId;
					// echo "New Candidate";
					// echo $candidateID;
				}

				if (count($resume)) {
					if (isset($resume['tmp_name']) && isset($resume['tmp_name'])) {
						while (true) {
							$candidateFile = [];
							$candidateFile['externalID'] = "portfolio";
							$candidateFile['fileContent'] = base64_encode(file_get_contents($resume['tmp_name']));
							$candidateFile['fileType'] = "SAMPLE";
							$ext = explode('.', $resume['name']);
							$candidateFile['name'] = 'resume-'.rand().'.'.end($ext);
							$candidateFile = ['json' => $candidateFile];

							try {
								$fileUp = $client->request(
									'PUT',
								    'file/Candidate/'.$candidateID,
								    $candidateFile
								);
								print_r($fileUp);
								if ($fileUp != '') {
									break;
								}
							} catch (Exception $e) {
								$response = 'Something wrong with bullhorn connection please make sure credentials are correct.';
								$response .= 'Error Code: ' .$e->getCode();
								$response .= '. Error Message: ' .$e->getMessage();
								print_r($response);
							}
						}
					}
				}

				$jobSubmission = [];
				$jobSubmission['status'] = 'New Lead';
				$jobSubmission['dateWebResponse'] = strtotime(date('Y-m-d h:i:s'));
				$jobSubmission['candidate'] = [
					'id' => $candidateID
				];
				$jobSubmission['jobOrder'] = [
					'id' => $job_id
				];
				$jobSubmission = ['json' => $jobSubmission];

				$response = $client->request(
					'PUT',
				    'entity/JobSubmission',
				    $jobSubmission
				);

				$response = $job_id;

				break;
			} catch (Exception $e) {
				if ($e->getCode() != 500) {
					$response = 'Something wrong with bullhorn connection please make sure credentials are correct.';
					$response .= 'Error Code: ' .$e->getCode();
					$response .= '. Error Message: ' .$e->getMessage();
					break;
				}
			}
			usleep(200000);
		}

		return $response;
	}

	public function submit_application_form() {

		$address = new stdClass();
		$category = new stdClass();

		$candidate = [];

		$candidate['name'] = $_POST['first_name']. ' ' . $_POST['middle_name'] . ' '. $_POST['last_name'];
		$candidate['firstName'] = $_POST['first_name'];
		$candidate['middleName'] = $_POST['middle_name'];
		$candidate['lastName'] = $_POST['last_name'];
		$candidate['status'] = 'New Lead';
		$candidate['source'] = $_POST['source'];
		$candidate['customText8'] = ($_POST['source_other'] != '') ? $_POST['source_other'] : '';
		$candidate['mobile'] = $_POST['mobile'];
		$candidate['email'] = $_POST['email'];

		$address->address1 = $_POST['address1'];
		$address->address2 = ($_POST['address2'] != '') ? $_POST['address2'] : '';
		$address->city = $_POST['city'];
		$address->state = ($_POST['state_other'] != '') ? $_POST['state_other'] : $_POST['state'];
		$address->zip = $_POST['zip'];
		$address->countryName = $_POST['country'];
		$candidate['address'] = $address;
		
		$catVal = explode("|",$_POST['category']);
		$category = new stdClass();
		$category->id = $catVal[0];
		$category->name = $catVal[1];
		$candidate['category'] = $category;

		$industry = new stdClass();
		$industry->id = '1100058';
		$industry->name = 'BlueForce';
		$candidate['businessSectorID'] = 'BlueForce';

		$candidate['job_id'] = $_POST['job_id'];
		$candidate['resume'] = '';
		if (isset($_FILES['resume'])) {
			$resume = $_FILES['resume'];
			$candidate['resume'] = $resume;

			if ($resume['type'] != 'application/msword' && $resume['type'] != 'application/pdf' && $resume['type'] != 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
				echo 'Allowed file types are .doc, .pdf and .docx';
				exit();
			}
		}


		$response = self::create_candidate($candidate);
		// echo $response;
		exit();

	}

	public function candidate_check_form() {

		$candidate = [];
		$candidate['name'] = $_POST['first_name']. ' ' . $_POST['middle_name'] . ' '. $_POST['last_name'];
		$candidate['firstName'] = $_POST['first_name'];
		$candidate['middleName'] = $_POST['middle_name'];
		$candidate['lastName'] = $_POST['last_name'];
		$candidate['email'] = $_POST['email'];
		$job_id = $_POST['job_id'];

		$candidateID = self::search_candidate($candidate['email']);

		if ($candidateID) {
			$response = '';
			while (true) {
				try {
					$client = self::authentication(
						get_option('bh2wp_client_id'),
						get_option('bh2wp_client_secret'),
						get_option('bh2wp_api_username'),
						get_option('bh2wp_api_password')
					);

					$jobSubmission = [];
					$jobSubmission['status'] = 'New Lead';
					$jobSubmission['dateWebResponse'] = strtotime(date('Y-m-d h:i:s'));
					$jobSubmission['candidate'] = [
						'id' => $candidateID
					];
					$jobSubmission['jobOrder'] = [
						'id' => $job_id
					];
					$jobSubmission = ['json' => $jobSubmission];

					$response = $client->request(
						'PUT',
					    'entity/JobSubmission',
					    $jobSubmission
					);

					$response = $job_id;

					break;
				} catch (Exception $e) {
					if ($e->getCode() != 500) {
						$response = 'Something wrong with bullhorn connection please make sure credentials are correct.';
						$response .= 'Error Code: ' .$e->getCode();
						$response .= '. Error Message: ' .$e->getMessage();
						break;
					}
				}
				usleep(200000);
			}

			echo $response;
		} else {
			echo 'no candidate found';
		}
		exit();

	}

	public function view_application_form() {
		echo ' <div class="bh2wp"><div id="bh2wp-apply-now-form-backdrop"></div>
			<div id="bh2wp-apply-now-form">
				<div class="bh2wp-apply">
					<small>You are applying for:</small>
					<h3 id="positionApplying"></h3>
					<form class="bh2wp-check-candidate-form" method="POST" enctype="multipart/form-data">
						<input type="hidden" id="input_job_id2" name="job_id" value="">
						<input type="hidden" name="action" value="candidate_check_form">
						<div class="bh_row">
							<div class="bh_col_3">
								<label>First Name</label>
								<input type="text" name="first_name" required="required">
							</div>
							<div class="bh_col_3 middlePad">
								<label>Middle Name</label>
								<input type="text" name="middle_name" required="required">
							</div>
							<div class="bh_col_3">
								<label>Last Name</label>
								<input type="text" name="last_name" required="required">
							</div>
							<br clear="all">
						</div>
						<div class="emailSec">
							<label>Email</label>
							<input type="email" name="email" required="required">
						</div>
						<input type="submit" value="Submit" name="submit"><img src="'.plugin_dir_url( __FILE__ ).'partials/images/loadloader.gif" class="showLoaderLoader" style="width: 28px; display: none; margin: -35px auto 12px auto;">
						<small class="successText"></small>
					</form>
					<form class="bh2wp-application-form" method="POST" enctype="multipart/form-data" style="display: none;">
						<input type="hidden" id="input_job_id" name="job_id" value="">
						<input type="hidden" name="action" value="submit_application_form">
						<input type="hidden" name="status" value="New Lead" required="required">
						<div class="bh_row">
							<div class="bh_col_3">
								<label>First Name</label>
								<input type="text" name="first_name" required="required">
							</div>
							<div class="bh_col_3 middlePad mobpad">
								<label>Middle Name</label>
								<input type="text" name="middle_name" required="required">
							</div>
							<div class="bh_col_3">
								<label>Last Name</label>
								<input type="text" name="last_name" required="required">
							</div>
							<br clear="all">
						</div>
						<div class="emailSec mobpad">
							<label>Email</label>
							<input type="email" name="email" required="required">
						</div>
						<small style="margin-bottom: 10px;">Please fill out some more information.</small>
						<div class="mobileResSec">
						<div class="bh_row">
							<div class="bh_col_3">
								<label>Phone</label>
								<input type="tel" name="mobile" required="required">
							</div>
							<div class="bh_col_3 middlePad mobpad">
								<label>Select Source</label>
								<select id="source_dropdown" name="source" required="required" style="padding-bottom: 8px; padding-top: 8px; font-size: 12px; padding-left: 8px; padding-right: 0px;">
									<option value=""></option>
									<option value="Automated Call">Automated Call</option>
									<option value="Company Website">Company Website</option>
									<option value="Employee Referral">Employee Referral</option>
									<option value="Google Search">Google Search</option>
									<option value="Job Board">Job Board</option>
									<option value="Newspaper">Newspaper</option>
									<option value="Postcard">Postcard</option>
									<option value="Social Media">Social Media</option>
									<option value="Other">Other</option>
								<select>
							</div>
							<div class="bh_col_3">
								<label class="optional">Source Other</label>
								<input id="source_other" type="text" name="source_other">
							</div>
							<br clear="all">
						</div>
						<div class="bh_row">
							<div class="bh_col_2 rightPad mobpad" style="width: 67%;">
								<label>Address</label>
								<input type="text" name="address1" required="required">
							</div>
							<div class="bh_col_2" style="width: 33%;">
								<label class="optional">Apt</label>
								<input type="text" name="address2">
							</div>
							<br clear="all">
						</div>
						<div class="bh_row">
							<div class="bh_col_3 mobpad">
								<label>City</label>
								<input type="text" name="city" required="required">
							</div>
							<div class="bh_col_3 middlePad">
								<label>Select State</label>
								<select name="state" id="state_dropdown" required="required">
									<option value=""></option>
									<option value="Alabama">Alabama</option>
									<option value="Alaska">Alaska</option>
									<option value="Arizona">Arizona</option>
									<option value="Arkansas">Arkansas</option>
									<option value="California">California</option>
									<option value="Colorado">Colorado</option>
									<option value="Connecticut">Connecticut</option>
									<option value="Delaware">Delaware</option>
									<option value="District of Columbia">District of Columbia</option>
									<option value="Florida">Florida</option>
									<option value="Georgia">Georgia</option>
									<option value="Hawaii">Hawaii</option>
									<option value="Idaho">Idaho</option>
									<option value="Illinois">Illinois</option>
									<option value="Indiana">Indiana</option>
									<option value="Iowa">Iowa</option>
									<option value="Kansas">Kansas</option>
									<option value="Kentucky">Kentucky</option>
									<option value="Louisiana">Louisiana</option>
									<option value="Maine">Maine</option>
									<option value="Maryland">Maryland</option>
									<option value="Massachusetts">Massachusetts</option>
									<option value="Michigan">Michigan</option>
									<option value="Minnesota">Minnesota</option>
									<option value="Mississippi">Mississippi</option>
									<option value="Missouri">Missouri</option>
									<option value="Montana">Montana</option>
									<option value="Nebraska">Nebraska</option>
									<option value="Nevada">Nevada</option>
									<option value="New Hampshire">New Hampshire</option>
									<option value="New Jersey">New Jersey</option>
									<option value="New Mexico">New Mexico</option>
									<option value="New York">New York</option>
									<option value="North Carolina">North Carolina</option>
									<option value="North Dakota">North Dakota</option>
									<option value="Ohio">Ohio</option>
									<option value="Oklahoma">Oklahoma</option>
									<option value="Oregon">Oregon</option>
									<option value="Pennsylvania">Pennsylvania</option>
									<option value="Rhode Island">Rhode Island</option>
									<option value="South Carolina">South Carolina</option>
									<option value="South Dakota">South Dakota</option>
									<option value="Tennessee">Tennessee</option>
									<option value="Texas">Texas</option>
									<option value="Utah">Utah</option>
									<option value="Vermont">Vermont</option>
									<option value="Virginia">Virginia</option>
									<option value="Washington">Washington</option>
									<option value="West Virginia">West Virginia</option>
									<option value="Wisconsin">Wisconsin</option>
									<option value="Wyoming">Wyoming</option>
								</select>
							</div>
							<div class="bh_col_3 mobpad">
								<label>Zip Code</label>
								<input type="text" name="zip" required="required">
							</div>
							<br clear="all">
						</div>
						<div>
						<label>Select Country</label>
						<select name="country" id="country_dropdown" required="required">
							<option value="Canada">Canada</option>
							<option value="United States" selected="selected">United States</option>
						</select>
						</div>
						<div class="mobpad">
						<label>Select Your Job Category</label>
						<select id="category" name="category" required="required">
							<option value="2000003|Allied Health Professional">Allied Health Professional</option>
							<option value="2000002|Certified Nursing Assistant (CNA)">Certified Nursing Assistant (CNA)</option>
							<option value="2000328|Crisis Security Officer">Crisis Security Officer</option>
							<option value="2000006|Industrial">Industrial</option>
							<option value="2000001|Licensed Practical / Vocational Nurse (LPN / LVN)">Licensed Practical / Vocational Nurse (LPN / LVN)</option>
							<option value="2000447|Other">Other</option>
							<option value="2000000|Registered Nurse (RN)">Registered Nurse (RN)</option>
							<option value="2000771|Service Health Professional">Service Health Professional</option>
						</select>
						</div>
						<br clear="all">
						</div>
						<label class="optional">Upload Resume</label>
						<div class="selectedResume" style="margin-bottom:20px;">
							<div class="resumeInput"><input type="file" name="resume"></div>
							<div class="resumeDelete" style="display: none; margin-top: 5px;"><span style="height: 30px; background: #f4f3f3; border-radius: 3px; padding: 5px 10px; margin-right: 10px;"><a href="javascript:void(0);" id="nananame" style="color: #4b4c57; font-size: 14px; font-weight: 400; text-decoration: none;">Combined_Shape.png</a> <img src="'.plugin_dir_url( __FILE__ ).'partials/images/Combined_Shape.png" style="vertical-align: middle; margin-top: -3px;"></span><a id="resumeDeleteBtn" href="javascript:void(0);" style="color: #326dfb; font-size: 13px; font-weight: bold; text-decoration: none;">Delete</a></div>
						</div>
						<input type="submit" value="Submit" name="submit"><img src="'.plugin_dir_url( __FILE__ ).'partials/images/loadloader.gif" class="showLoaderLoader" style="width: 28px; display: none; margin: -35px auto 12px auto;">
						<small class="successText"></small>
					</form>
				</div>
				<div id="successMessage" style="display: none;">
					<img src="'.plugin_dir_url( __FILE__ ).'partials/images/thanks-sign.png" style="display: block; margin: 0 auto 20px auto;">
					<h1>Thank you!<br>Your application has been submitted!</h1>
					<a href="javascript:void(0);" id="thanksCancelBtn">Cancel</a>
				</div><a href="javascript:void(0);" id="popUpCloseBtn"><img src="'.plugin_dir_url( __FILE__ ).'partials/images/cross-sign.png"></a>
			</div></div>
		';
	}
	
	public function save_job_alerts() {

		global $wpdb;

		$save_info = [];
		$save_info['subscriber_name'] = $_POST['subscriber_name'];
		$save_info['subscriber_email'] = $_POST['subscriber_email'];
		$save_info['search_keyword'] = $_POST['search_keyword'];
		$save_info['search_location'] = $_POST['search_location'];
		$save_info['random_id'] = uniqid();

		// if (is_multisite()) {
		// 	$current_site = get_current_site();
			// $table_name = $wpdb->prefix . $current_site->id . '_job_alerts';
		// } else {
			$table_name = $wpdb->prefix . 'job_alerts';
		// }
		
		$subsEmail = $save_info['subscriber_email'];
		$subsLocation = $save_info['search_location'];
		$subsKeyword = $save_info['search_keyword'];
		$alreadySubscribed = $wpdb->get_results( "SELECT * FROM $table_name WHERE subscriber_email = '$subsEmail' AND search_keyword = '$subsKeyword' AND search_location = '$subsLocation'");

		if (count($alreadySubscribed)) {
			echo 'already';
			exit();
		}

		$insertID = $wpdb->insert( 
			$table_name,
			$save_info 
		);

		echo $insertID;

		exit();

	}
	
	public function bh2wp_schedule_cron() {
		if ( !wp_next_scheduled( 'bh2wp_cron' ) )
			wp_schedule_event(time(), 'daily', 'bh2wp_cron');
	}
	
	public function send_job_alerts() {
		
		global $wpdb;

			// update_site_option('send_job_alerts', '');
// 		if (is_multisite()) {
// 			$current_site = get_current_site();
// 			$tablename = $wpdb->prefix . $current_site->id . '_job_alerts';
// 		} else {
			$tablename = $wpdb->prefix . 'job_alerts';
// 		}

		$bhOptions = $GLOBALS['shortcode_values'];

// update_site_option('send_job_alerts', 'ahsan');
		// if (is_multisite()) {
		// 	$timeRan = get_site_option('send_job_alerts').'000';
		// } else {
			$timeRan = get_option('send_job_alerts').'000';
		// }
		
// print_r($timeRan); die;
		$allJobs = self::get_all_jobs('1100060,1100058,1100062,1100061', '2000003,2000002,2000001,2000000,2000328,2000006,2000771,2000447');
		
		$refineJobs = [];
		$searchIn = [];

		$allSubscriber = $wpdb->get_results( "SELECT * FROM $tablename");
// print_r($allSubscriber);
// die;
		foreach ($allSubscriber as $key => $subscriber) {

			$matchedTitle = [];
			$matchedLocation = [];
			$matchedJobsAll = [];
			
			foreach ($allJobs->data as $key => $job) {

				if ($job->dateLastModified > $timeRan) {
				// if ($subscriber->subscriber_email == 'romant@addiedigital.com' || $subscriber->subscriber_email == 'ahsanrehman321@gmail.com') {
				// if ($subscriber->subscriber_email == 'ahsanrehman321@gmail.com') {
					// if ($subscriber->search_location != '') {
					// 	if ($job->address->state == $subscriber->search_location) {
					// 		$matchedLocation[] = $job;
					// 	}
					// }

					// if ($subscriber->search_keyword != '') {
					//     $jobTitle = '';
					// 	if(isset($job->skills->data[0])) {
					// 		$jobTitle = $job->skills->data[0]->name;
					// 	} elseif (isset($job->categories->data[0])) {
					// 		$jobTitle = $job->categories->data[0]->name;
					// 	} else {
					// 		$jobTitle = $job->title;
					// 	}
					// 	if($bhOptions['show_cat'] == 'true') {
					// 		if(isset($job->specialties->data[0])) {
					// 			$jobTitle .= ', '.$job->specialties->data[0]->name;
					// 		}
					// 	}

					// 	if (strpos(strtolower($jobTitle), strtolower($subscriber->search_keyword)) !== false) {
					// 		$matchedTitle[] = $job;
					// 	}
					// }
					


					if ($subscriber->search_location == '' && $subscriber->search_keyword != '') {
						// print_r($subscriber);
					    $jobTitle = '';
						if(isset($job->skills->data[0])) {
							$jobTitle = $job->skills->data[0]->name;
						} elseif (isset($job->categories->data[0])) {
							$jobTitle = $job->categories->data[0]->name;
						} else {
							$jobTitle = $job->title;
						}
						// if($bhOptions['show_cat'] == 'true') {
							if(isset($job->specialties->data[0])) {
								$jobTitle .= ', '.$job->specialties->data[0]->name;
							}
						// }

						if (strpos(strtolower($jobTitle), strtolower($subscriber->search_keyword)) !== false) {
							// $matchedTitle[] = $job;
							$matchedJobsAll[$job->id] = $job;
						}

						$emailAlertHeading = $subscriber->search_keyword;
					}
					
					if ($subscriber->search_location != '' && $subscriber->search_keyword == '') {
						if ($job->address->state == $subscriber->search_location) {
							// $matchedLocation[] = $job;
							$matchedJobsAll[$job->id] = $job;
						}
						$emailAlertHeading = $subscriber->search_location;
					}

					if ($subscriber->search_location != '' && $subscriber->search_keyword != '') {

						if ($job->address->state == $subscriber->search_location) {

						    $jobTitle = '';
							if(isset($job->skills->data[0])) {
								$jobTitle = $job->skills->data[0]->name;
							} elseif (isset($job->categories->data[0])) {
								$jobTitle = $job->categories->data[0]->name;
							} else {
								$jobTitle = $job->title;
							}
							// if($bhOptions['show_cat'] == 'true') {
								if(isset($job->specialties->data[0])) {
									$jobTitle .= ', '.$job->specialties->data[0]->name;
								}
							// }

							if (strpos(strtolower($jobTitle), strtolower($subscriber->search_keyword)) !== false) {
								// $matchedTitle[] = $job;
								$matchedJobsAll[$job->id] = $job;
							}
							// $matchedLocation[] = $job;
						}

						$emailAlertHeading = $subscriber->search_keyword.', '.$subscriber->search_location;
					}

				}

			}  
// print_r($matchedJobsAll);
// print_r($matchedTitle);
// echo "<br>";
// echo "<br>";
// die;
// $totalJobsCount = count($matchedTitle) + count($matchedLocation);
// print_r($matchedJobsAll);
// die;
$totalJobsCount = count($matchedJobsAll);
$htmlTemplate = '<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body, html {margin: 0; padding: 0;}
		table {margin: 0 auto;}
		.jobsLink a {text-decoration: none !important;}
		.jobsLink a:hover td {color: #326dfb !important;}
		.jobsLink td:hover a {color: #326dfb !important;}
		.jobsLink a {color: #222 !important;}
		.jobsLink a:hover {color: #326dfb !important;}
		.jobsLink a:hover td a {color: #326dfb !important;}
		.jobsLink table:hover td a {color: #326dfb !important;}
	</style>
</head>
<body>
	<table cellpadding="0" cellspacing="0" width="600">
		<tr>
			<td><br><a href="'.get_bloginfo('url').'"><img src="'.plugin_dir_url( __FILE__ ).'partials/images/Original_Logo.png" width="300" height="auto"></a><br><br></td>
		</tr>
		<tr>
			<td bgcolor="#f8f8f8" style="padding: 5px 15px; color: #0f0f0f; font-family: Arial; font-size: 14px; line-height: 22px;">
				<p>Hi '.$subscriber->subscriber_name.',<br>Huffmaster has new opportunities available that match your job alert. Click on a listing below to learn more about the opportunity and to apply.</p>
			</td>
		</tr>
		<tr>
			<td>
				<br>
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td align="left" style="color: #0f0f0f; font-family: Arial; font-size: 14px; font-weight: 700;">'.$emailAlertHeading.'</td>
						<td align="right" style="color: #8a8c9b; font-family: Arial; font-size: 12px; font-weight: 700;">Positions Matched: '.$totalJobsCount.'</td>
					</tr>
				</table>
				<br>
			</td>
		</tr>
		<tr>
			<td class="jobsLink" style="border-bottom: 1px solid #cccccc;">';
foreach ($matchedJobsAll as $key => $value) {
$jobTitle2 = '';
if(isset($value->skills->data[0])) {
	$jobTitle2 = $value->skills->data[0]->name;
} elseif (isset($value->categories->data[0])) {
	$jobTitle2 = $value->categories->data[0]->name;
} else {
	$jobTitle2 = $value->title;
}
// if($bhOptions['show_cat'] == 'true') {
	if(isset($value->specialties->data[0])) {
		$jobTitle2 .= ', '.$value->specialties->data[0]->name;
	}
// }
$htmlTemplate .= '<a href="'.get_bloginfo('url')."/all-open-jobs/?job=".$value->id.'" target="_blank" style="text-decoration: none;">
					<table cellpadding="0" cellspacing="0" width="100%" style="padding: 10px; border: 1px solid #cccccc; border-width: 1px 1px 0px 1px; background: #f8f8f8;">
						<tr>
							<td align="left" style="color: #222222; font-family: Arial; font-size: 18px; font-weight: 700;"><a href="'.get_bloginfo('url')."/all-open-jobs/?job=".$value->id.'" target="_blank" style="text-decoration: none;">'.$jobTitle2.'</a></td>
							<td rowspan="2" valign="center" align="right"><a href="'.get_bloginfo('url')."/all-open-jobs/?job=".$value->id.'" target="_blank" style="text-decoration: none;"><img src="'.get_bloginfo('url').'/wp-content/uploads/2018/11/Rectangle_4.png"></a></td>
						</tr>
						<tr>
							<td align="left" style="color: #222222; font-family: Verdana, Arial; font-size: 12px;"><a href="'.get_bloginfo('url')."/all-open-jobs/?job=".$value->id.'" target="_blank" style="text-decoration: none;">Location: '.$value->address->state.'</a></td>
						</tr>
					</table>
				</a>';
}
// foreach ($matchedLocation as $key => $value) {
// $htmlTemplate .= '<a href="'.get_bloginfo('url')."/all-open-jobs/?job=".$value->id.'" target="_blank" style="text-decoration: none;">
// 					<table cellpadding="0" cellspacing="0" width="100%" style="padding: 10px; border: 1px solid #cccccc; border-width: 1px 1px 0px 1px; background: #f8f8f8;">
// 						<tr>
// 							<td align="left" style="color: #222222; font-family: Arial; font-size: 18px; font-weight: 700;">'.$value->title.'</td>
// 							<td rowspan="2" valign="center" align="right"><img src="'.get_bloginfo('url').'/wp-content/uploads/2018/11/Rectangle_4.png"></td>
// 						</tr>
// 						<tr>
// 							<td align="left" style="color: #222222; font-family: Verdana, Arial; font-size: 12px;">Location: '.$value->address->state.'</td>
// 						</tr>
// 					</table>
// 				</a>';
// }
$htmlTemplate .= '</td>
		</tr>
		<tr>
			<td>
				<br>
				<a href="'.get_bloginfo('url').'/unsubscribe/?user='.$subscriber->random_id.'" target="_blank" style="text-decoration: none; color: #326dfb; font-family: Arial; font-size: 12px;">Unsubscribe from this mailing list.</a>
				<br>
			</td>
		</tr>
	</table>
	<br>
	<div style="background: #1e2447">
		<table cellpadding="0" cellspacing="0" width="600" bgcolor="#1e2447">
			<tr>
				<td style="color: #8a8c9b; font-family: Arial; font-size: 10px;">
					<br>
					&nbsp; &nbsp; <a href="'.get_bloginfo('url').'"><img src="'.plugin_dir_url( __FILE__ ).'partials/images/Logo.png" width="222" height="auto"></a>
					<br><br>&nbsp; &nbsp; &copy; Copyright '.date('Y').' Huffmaster. All Rights Reserved.<br>
					<br>
				</td>
				<td style="width: 140px;">
					<p style="color: #8a8c9b; font-family: Arial; font-size: 10px; font-weight: 700; margin: 0;">Corporate Headquarters</p>
					<p style="color: #ffffff; font-family: Arial; font-size: 12px; font-weight: 700;">1055 W Maple Rd.<br>Clawson, MI 48017</p>
				</td>
				<td>
					<p style="color: #8a8c9b; font-family: Arial; font-size: 10px; font-weight: 700; margin: 0;">Contact</p>
					<p style="color: #ffffff; font-family: Arial; font-size: 12px; font-weight: 700;">+1 (800) 446-1515<br>+1 (248) 588-1600 (Outside the US)</p>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>';
			$matchedJobs = $htmlTemplate;
// 			foreach ($matchedTitle as $key => $value) {
// 				$matchedJobs .= get_bloginfo('url')."/all-open-jobs/?job=".$value->id."
// "; 
// 			}

// 			foreach ($matchedLocation as $key => $value) {
// 				$matchedJobs .= get_bloginfo('url')."/all-open-jobs/?job=".$value->id."
// "; 
// 			}
			
			if ($matchedJobs != '') {
				$to = $subscriber->subscriber_email;
				$from = "info@huffmaster.com";
				$subject = "Huffmaster Job Alert";

			    $headers = array();
			    $headers[] = 'Content-Type: text/html; charset=UTF-8';
			    $headers[] = "From: ". $from;

			    $message =  $matchedJobs;
			    if ($totalJobsCount > 0) {
			    	wp_mail($to, $subject, $message, $headers);
			    }
// 				// print_r($matchedJobs);
// 				print_r($message);
// 				echo "<br>";
// 			print_r($message); echo "<br>";
			}
		}

// die;
		// if (is_multisite()) {
		// 	update_site_option('send_job_alerts', strtotime("now"));
		// } else {
			update_option('send_job_alerts', strtotime("now"));
		// }

	}

	public static function unsubscribe_shortcode( $atts ) {
		$content = '';
		if (isset($_POST['submit'])) {
			global $wpdb;
			$wpdb->delete( $wpdb->prefix . 'job_alerts', array( 'random_id' => $_POST['user'] ) );
			$content = '<div class="bh2wp_unsubscribe_shortcode_form"><p style="color: #000000; font-size: 24px; font-weight: 700; line-height: 43px;">Are you sure you’d like to stop receiving job email newsletters from Huffmaster?</p><div style="color: #222222; font-size: 16px; font-weight: 400; line-height: 22px; border-radius: 5px; background-color: #f8f8f8; padding: 15px;"><p style="margin: 0;">You have been unsubscribed.</p></div></div>';
		}
		if ($_GET['user']) {
			global $wpdb;
			$content = '<div class="bh2wp_unsubscribe_shortcode_form"><p style="color: #000000; font-size: 24px; font-weight: 700; line-height: 43px;">Are you sure you’d like to stop receiving job email newsletters from Huffmaster?</p><form action="'.get_permalink().'" method="post" class="unsubscribe_shortcode_form" style="margin: 0;"><input type="hidden" value="'.$_GET['user'].'" name="user"><input type="submit" name="submit" value="Unsubscribe" style="color: #ffffff; font-size: 16px; font-weight: 700; letter-spacing: -0.4px; border-radius: 23px; background-color: #ef0e40; width: 180px; border: 0px; padding: 15px;"></form></div>';
		}
		return $content;
	}
}
