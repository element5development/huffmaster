<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://ahsanrehman.com
 * @since      1.0.0
 *
 * @package    Bullhorn_To_Wordpress
 * @subpackage Bullhorn_To_Wordpress/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div id="bh2wp-main" class="bh2wp">
	<?php if ($bhOptions['logo']): ?>
	<div class="bh2wp-logo">
		<img src="<?php echo $bhOptions['logo']; ?>" style="max-width: 300px; margin-bottom: 20px;">
	</div>
	<?php endif ?>
	<div class="subscribeDiv">
		<a href="#" id="showSubscribeForm">
			<h3 style="color: #4b4c57; font-family: 'Roboto Medium'; font-size: 18px; font-weight: bold; line-height: 22px;">Subscribe for job alerts!</h3>
			<div class="subsMainBtn">Subscribe</div>
		</a>
		<form id="subscribeForm" style="display: none;">
			<div>
				<div style="padding-right: 10px;">
					<label>Your Name</label>
					<input type="text" name="subscriber_name" required="required">
				</div>
				<div style="padding-left: 10px;">
					<label>Your Email</label>
					<input type="email" name="subscriber_email" required="required">
				</div>
				<div style="width: 100%; position: relative;">
					<img src="<?php echo plugin_dir_url( __FILE__ ); ?>images/loadloader.gif" class="showLoaderLoader2" style="position: absolute; top: 28px; width: 30px; left: 34px; display: none;">
					<input type="submit" name="submit" value="Save">
					<a href="javascript:void(0);" id="showSubscribeFormCancel">Cancel</a>
					<small class="successText"></small>
				</div>
				<br clear="all">
			</div>
		</form>
	</div>
	<div class="searchingDiv">
		<div class="searchByKeyword">
			<label class="optional">Search by Keyword</label>
			<input type="text" id="filterKeyword" value="">
		</div>
		<div class="searchByLocation">
			<label class="optional">Search by Location</label>
			<select id="filterLocation">
				<option value=""></option>
				<?php
				if(!in_array("Travel Throughout the US", array_unique($locations))) {
					echo '<option value="Travel Throughout the US">Travel Throughout the US</option>';
				}
				 foreach (array_unique($locations) as $key => $value) {
					echo '<option value="'.$value.'">'.$value.'</option>';
				} ?>
			</select>
		</div>
		<br clear="all">
	</div>
	<div class="sortingDiv">
		<div class="orderOption">
			<label>Order</label>
			<select id="order">
				<!-- <option value="">Order</option> -->
				<option value="asc" selected="selected">Ascending</option>
				<option value="desc">Descending</option>
			</select>
		</div>
		<div class="sortOption">
			<label>Sort by</label>
			<select id="sortBy">
				<!-- <option value="">Sort By</option> -->
				<option value="title" selected="selected">Title</option>
				<option value="location">Location</option>
			</select>
		</div>
		<br clear="all">
	</div>
	<div class="newOption bh2wpDesktop">
		<label class="optional" style="font-size: 16px; color: #4b4c57;">
			<input type="checkbox" class="filterNew" value="Yes"> New (7 Days)
		</label>
	</div>
	<div class="totalCount">
		<h3>Positions Matched: <span id="countNumber"><?php //echo $jobs->total; ?></span></h3>
	</div>
<?php

if (isset($jobs->total) && $jobs->total > 0) {
	?>
	<table id="jobsTable" class="table bh2wp-table">
		<thead>
			<tr>
				<th>Job</th>
				<th>Title</th>
				<th>Location</th>
				<th>New</th>
			</tr>
		</thead>
		<tbody>
<?php
	foreach ($jobs->data as $key => $job) {
		// print_r($job);
		// echo "<br>";
		// echo "<br>";
?>
		<tr>
			<td id="bh<?php echo $job->id; ?>" class="bh2wp-section">
				<h3 class="bh2wp-clickable jobparatrig" data-jobid="<?php echo $job->id; ?>" style="margin: 0px;">
				
					<?php //echo $job->title; ?>
					<?php
					    $jobTitle = '';
		if(isset($job->skills->data[0])) {
			$jobTitle = $job->skills->data[0]->name;
		} elseif (isset($job->categories->data[0])) {
			$jobTitle = $job->categories->data[0]->name;
		} else {
			$jobTitle = $job->title;
		}
		// if($bhOptions['show_cat'] == 'true') {
			if(isset($job->specialties->data[0])) {
				$jobTitle .= ', '.$job->specialties->data[0]->name;
			}
		// }
					    /*if (isset($job->skills->data[0]) && isset($job->categories->data[0])) {
							if($job->categories->data[0]->name == 'Other') {
					        	$jobTitle = $job->skills->data[0]->name;
							} else {
					        	$jobTitle = $job->categories->data[0]->name.', '.$job->skills->data[0]->name;
							}
					    } elseif (isset($job->skills->data[0])) {
					        $jobTitle = $job->skills->data[0]->name;
					    } elseif (isset($job->categories->data[0])) {
					        $jobTitle = $job->categories->data[0]->name;
					    } else {
					        $jobTitle = $job->title;
					    }*/
					?>
					<?php echo $jobTitle; ?>
				</h3>
				<span class="bh2wp-clickable bh2wp-clickable-location" data-jobid="<?php echo $job->id; ?>">Location: <?php echo ($job->address->state) ? $job->address->state : 'Travel Throughout the US'; ?></span>
				<div class="bh2wp-view">
					<?php if ($job->correlatedCustomFloat3): ?>
						<strong>Weekly Gross:</strong>
						<h3><?php echo $job->correlatedCustomFloat3; ?></h3>
					<?php endif ?>
					<?php if ($job->customTextBlock1): ?>
						<strong>Requirements:</strong>
						<p><?php echo $job->customTextBlock1; ?></p>
					<?php endif ?>
					<?php if ($job->description): ?>
						<strong>Description:</strong>
						<?php if (strlen($job->description) > 4): ?>
							<p><?php echo $job->description; ?></p>
						<?php else : ?>
							<p>No description found.</p>
						<?php endif ?>
					<?php endif ?>
					<div class="bh2wp-buttons">
						<a href="mailto:?subject=This <?php echo $job->title; ?> position might interest you&body=I found this position that I wanted to pass along to you:%0D%0A%0D%0ATitle: <?php echo $job->title; ?>%0D%0ACompany: Huffmaster Companies%0D%0A%0D%0A%0D%0AClick the link below to see the details of the position:%0D%0A<?php echo get_the_permalink().'?job='.$job->id; ?>%0D%0A" class="bh2wp-show-email" style="background: #326dfb; border-color: #326dfb;">Email</a>
						<a href="javascript:;" class="bh2wp-show-apply" data-jobtitle="<?php echo $jobTitle; ?>" data-jobid="<?php echo $job->id; ?>" style="background: #ef0e40; border-color: #ef0e40;">Apply</a>
					</div>
				</div>
				<span class="showToggleText">Show More</span>
			</td>
			<td><?php echo $jobTitle; ?></td>
			<td><?php echo ($job->address->state) ? $job->address->state : 'Travel Throughout the US'; ?></td>
			<td>
				<?php 
					$sevenDays = strtotime('-7 days');
					$modified = $job->dateLastModified/1000;
					if ($modified >= $sevenDays) {
						echo 'Yes';
					} else {
						echo 'No';
					}
				?>	
			</td>
		</tr>
<?php
	}
	echo '<tbody></table>';
} else {

	if (is_object($jobs)) {
		echo "<br><br><br>No jobs found";
	} else {
		echo '<br><br><br>No jobs found. '.$jobs;
	}

}
?>
<?php if (isset($_GET['job'])) { ?>
<script type="text/javascript">
	jQuery(function($){
		$('html, body').animate({
	        scrollTop: $('#bh<?php echo $_GET["job"]; ?>').offset().top-100
	    }, 1000);
		$('#bh<?php echo $_GET["job"]; ?>').find('.jobparatrig').trigger('click');
		$('#bh<?php echo $_GET["job"]; ?>').find('.bh2wp-show-view').trigger('click');
		$('#bh<?php echo $_GET["job"]; ?>').find('#input_job_id').val(<?php echo $_GET["job"]; ?>);
	});
</script>
<?php } ?>
</div>


<style type="text/css">
	#bh2wp-main {
		/*border-color: <?php echo $bhOptions['color']; ?>*/
	}
	#bh2wp-main .bh2wp-clickable-location,
	#bh2wp-main .bh2wp-clickable-location small,
	/*.bh2wp .subscribeDiv h3,*/
	#bh2wp-main .totalCount h3 {
		/*color: <?php echo $bhOptions['color']; ?>*/
	}
	.bh2wp .subscribeDiv input[type="submit"],
	.bh2wp input[type="submit"],
	.bh2wp .bh2wp-buttons td a {
		/*color: #fff;*/
		/*background: <?php echo $bhOptions['color']; ?>*/
	}
	.bh2wp select {
		background-image: url("<?php echo plugin_dir_url( __FILE__ ); ?>images/Rectangle_9_Copy_3.png");
		background-repeat: no-repeat;
		background-position: 96% 14px;
	}
	/*.bh2wp input[disabled="disabled"]:after {
		content: '';
		width: 48px;
		height: 48px;
		background-image: url("<?php echo plugin_dir_url( __FILE__ ); ?>images/loadloader.gif");
		background-repeat: no-repeat;
	}*/
</style>