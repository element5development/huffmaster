(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	$('#countNumber').html($('#jobsTable > tbody > tr').length);
	
    var jobsTable = $('#jobsTable').dataTable({
    	"paging": false,
    	"columnDefs": [{
            "targets": [ 1,2,3 ],
            "visible": false
        }]
    });


    $('#order').change( function() {
    	if ($('#sortBy').val() == 'location' && $(this).val() == 'asc') {
			jobsTable.fnSort( [ [2, 'asc'] ] );
    	} else if ($('#sortBy').val() == 'location' && $(this).val() == 'desc') {
			jobsTable.fnSort( [ [2, 'desc'] ] );
    	} else if ($('#sortBy').val() == 'title' && $(this).val() == 'asc') {
			jobsTable.fnSort( [ [1, 'asc'] ] );
    	} else if ($('#sortBy').val() == 'title' && $(this).val() == 'desc') {
			jobsTable.fnSort( [ [1, 'desc'] ] );
    	} else {
			jobsTable.fnSort( [ [0, this.value] ] );
    	}
	});

    $('#sortBy').change( function() {
    	if ($(this).val() == 'location' && $('#order').val() == 'asc') {
			jobsTable.fnSort( [ [2, 'asc'] ] );
    	} else if ($(this).val() == 'location' && $('#order').val() == 'desc') {
			jobsTable.fnSort( [ [2, 'desc'] ] );
    	} else if ($(this).val() == 'title' && $('#order').val() == 'asc') {
			jobsTable.fnSort( [ [1, 'asc'] ] );
    	} else if ($(this).val() == 'title' && $('#order').val() == 'desc') {
			jobsTable.fnSort( [ [1, 'desc'] ] );
    	} else {
			jobsTable.fnSort( [ [0, 'asc'] ] );
    	}
	});

    $('#filterLocation').change( function() {
    	if (this.value) {
			jobsTable.fnFilter( this.value, 2 );
    	} else {
	        jobsTable.fnFilter( '', 2 );
	    }
		if ($('#jobsTable > tbody > tr').length == 1) {
			var theText = $('#jobsTable > tbody > tr').find('.dataTables_empty').length;
			if (theText == 1) {
				$('#countNumber').html('0');
			} else {
				$('#countNumber').html($('#jobsTable > tbody > tr').length);
			}
		} else {
			$('#countNumber').html($('#jobsTable > tbody > tr').length);
		}
	});

	$('#filterKeyword').keyup(function(){
		jobsTable.fnFilter( $(this).val() );
		if ($('#jobsTable > tbody > tr').length == 1) {
			var theText = $('#jobsTable > tbody > tr').find('.dataTables_empty').length;
			if (theText == 1) {
				$('#countNumber').html('0');
			} else {
				$('#countNumber').html($('#jobsTable > tbody > tr').length);
			}
		} else {
			$('#countNumber').html($('#jobsTable > tbody > tr').length);
		}
	});

	$('.filterNew').click(function(){
	    if($(this).is(':checked')){
	        jobsTable.fnFilter( this.value, 3 );
	    } else {
	        jobsTable.fnFilter( '', 3 );
	    }
		if ($('#jobsTable > tbody > tr').length == 1) {
			var theText = $('#jobsTable > tbody > tr').find('.dataTables_empty').length;
			if (theText == 1) {
				$('#countNumber').html('0');
			} else {
				$('#countNumber').html($('#jobsTable > tbody > tr').length);
			}
		} else {
			$('#countNumber').html($('#jobsTable > tbody > tr').length);
		}
	});

	$(document).on('click', '.bh2wp .bh2wp-table .bh2wp-clickable, .showToggleText', function(e){
		// $('.bh2wp .bh2wp-table .bh2wp-section').removeClass('bh2wp-selected');
		$(this).parent().toggleClass('bh2wp-selected');
		$('#input_job_id').val($(this).attr('data-jobid'));
		$('#input_job_id2').val($(this).attr('data-jobid'));
		$(this).parent().find('.bh2wp-view').slideToggle(400);
		if ($(this).parent().find('.showToggleText').text() == 'Show Less') {
			$(this).parent().find('.showToggleText').text('Show More');
		} else {
			$(this).parent().find('.showToggleText').text('Show Less');
		}
		e.preventDefault();
	});

	$(document).on('change', 'input[name="resume"]', function(e){
		var fileanme = $(this).val();
		if (fileanme != '') {
			$('#nananame').html($(this)[0].files[0]['name']);
			$('.resumeInput').hide();
			$('.resumeDelete').show();
		}
	});

	$(document).on('click', '#resumeDeleteBtn', function(e){
		$('input[name="resume"]').val('');
		$('#nananame').html('');
		$('.resumeInput').show();
		$('.resumeDelete').hide();
	});

	$(document).on('click', '.bh2wp .bh2wp-show-apply', function(e){
		$('.bh2wp .successText').html('');
		$('.bh2wp-apply').show();
		$('#successMessage').hide();
		$('#input_job_id').val($(this).attr('data-jobid'));
		$('#input_job_id2').val($(this).attr('data-jobid'));
		$('#positionApplying').html($(this).attr('data-jobtitle'));
		$('#bh2wp-apply-now-form-backdrop, #bh2wp-apply-now-form').fadeIn(300);
		e.preventDefault();
	});

	$(document).on('click', '#bh2wp-apply-now-form-backdrop, #thanksCancelBtn, #popUpCloseBtn', function(e){
		$('.bh2wp .successText').html('');
		$('.bh2wp-apply').show();
		$('#successMessage').hide();
		$('#bh2wp-apply-now-form-backdrop, #bh2wp-apply-now-form').fadeOut(300);
		e.preventDefault();
	});

	$(document).on('click', '#showSubscribeForm, #showSubscribeFormCancel', function(e){
		$('#subscribeForm, .subsMainBtn').slideToggle();
		e.preventDefault();
	});

    $(document).on('change', '#country_dropdown', function() {
    	if ($(this).val() != 'United States') {
	    	$('#state_dropdown').hide();
	    	$('#state_dropdown').before('<input id="state_other" type="text" placeholder="State *" name="state_other" required="required">')
    	} else {
	    	$('#state_dropdown').show();
    		$('#state_other').remove();
    	}
	});

	$(document).on('submit', '#subscribeForm', function(e){
		var form = $(this);
		var searchKeyword = $('.searchingDiv #filterKeyword').val();
		var searchLocation = $('.searchingDiv #filterLocation').val();

		if (isEmptyOrSpaces($('.searchingDiv #filterKeyword').val()) && isEmptyOrSpaces($('.searchingDiv #filterLocation').val())) {
			alert('Please search for anything to save.');
			return false;
		}

		form.find('.showLoaderLoader2').css('display', 'block');
		$(this).find('input[name="submit"]').attr('disabled', 'disabled');
		// $(this).find('.successText').html(' &nbsp; &nbsp; Saving...');

		var data = new FormData();
	    data.append("subscriber_name", $(this).find('input[name="subscriber_name"]').val());  
	    data.append("subscriber_email", $(this).find('input[name="subscriber_email"]').val());  
	    data.append("search_keyword", searchKeyword);  
	    data.append("search_location", searchLocation);  
	    data.append("action", "save_job_alerts");  

	    $.ajax({
	        type: 'POST',
	        url: settings.ajaxurl,
	        data: data,
	        contentType: false,
	        processData: false,
	        success: function(response){
            	form.find('input[name="submit"]').removeAttr('disabled');
            	form.find('.showLoaderLoader2').css('display', 'none');
				form.find('.successText').html('');
				if (response) {
					$('.subscribeDiv').addClass('successSubscribed');
					if (response == 'already') {
						$('.subscribeDiv').html('<h3 style="color: #4b4c57; font-size: 18px; font-weight: bold; line-height: 22px;">You have already subscribed with same parameters.</h3>');
					} else {
						$('.subscribeDiv').html('<h3 style="color: #4b4c57; font-size: 18px; font-weight: bold; line-height: 22px;">You have been successfully subscribed!</h3>');
					}
				} else {
					form.find('.successText').html('Something went wrong try again!');
				}
	        }
	    });

		return false;
	});

	$(document).on('submit', '.bh2wp-check-candidate-form', function(e){

		var form = $(this);

		$(this).find('input[name="submit"]').attr('disabled', 'disabled');
		$(this).find('.showLoaderLoader').css('display', 'block');
		$(this).find('input[name="submit"]').css('opacity', '0.1');
		// $(this).find('.successText').html(' &nbsp; &nbsp; Submitting...');

		var data = new FormData();
	    data.append("first_name", $(this).find('input[name="first_name"]').val());  
	    data.append("middle_name", $(this).find('input[name="middle_name"]').val());  
	    data.append("last_name", $(this).find('input[name="last_name"]').val());  
	    data.append("email", $(this).find('input[name="email"]').val());  
	    data.append("job_id", $(this).find('input[name="job_id"]').val());  
	    data.append("action", "candidate_check_form");  

	    $.ajax({
	        type: 'POST',
	        url: settings.ajaxurl,
	        data: data,
	        contentType: false,
	        processData: false,
	        success: function(response){
            	form.find('input[name="submit"]').removeAttr('disabled');
            	form.find('.showLoaderLoader').css('display', 'none');
            	form.find('input[name="submit"]').css('opacity', '1');
				form.find('.successText').html('');
				if (response != 'no candidate found') {
					$('.bh2wp-apply').hide();
					$('#successMessage').show();
					$('.bh2wp-application-form').parent().parent().removeClass('increase-height');
            		form.find('.successText').html(' &nbsp; &nbsp; Your Application has been submitted successfully!');
					$(".bh2wp-check-candidate-form")[0].reset();
					setTimeout(function(){
						form.find('.successText').html('');
					}, 5000);
				} else {
					$('.bh2wp-application-form').find('input[name="first_name"]').val(form.find('input[name="first_name"]').val());
					$('.bh2wp-application-form').find('input[name="last_name"]').val(form.find('input[name="last_name"]').val());
					$('.bh2wp-application-form').find('input[name="middle_name"]').val(form.find('input[name="middle_name"]').val());
					$('.bh2wp-application-form').find('input[name="email"]').val(form.find('input[name="email"]').val());
					form.hide();
					$('.bh2wp-application-form').show();
					$('.bh2wp-application-form').parent().parent().addClass('increase-height');
				}
	        }
	    });

	    return false;
	});

	$(document).on('submit', '.bh2wp-application-form', function(e){

		var form = $(this);

		$(this).find('input[name="submit"]').attr('disabled', 'disabled');
		$(this).find('.showLoaderLoader').css('display', 'block');
		$(this).find('input[name="submit"]').css('opacity', '0.1');
		// $(this).find('.successText').html(' &nbsp; &nbsp; Submitting...');

		var data = new FormData();
	    var file = $(this).find('input[name="resume"]');
	    var individual_file = file[0].files[0];
	    var state_other = $(this).find('input[name="state_other"]').val();
	    if (state_other == undefined) {
	    	var state_other = '';
	    }
	    var source_other = $(this).find('input[name="source_other"]').val();
	    if (source_other == undefined) {
	    	var source_other = '';
	    }
	    data.append("resume", individual_file);
	    data.append("status", $(this).find('input[name="status"]').val());  
	    data.append("first_name", $(this).find('input[name="first_name"]').val());  
	    data.append("middle_name", $(this).find('input[name="middle_name"]').val());  
	    data.append("last_name", $(this).find('input[name="last_name"]').val());  
	    data.append("source", $(this).find('select[name="source"]').val());  
	    data.append("source_other", source_other);  
	    data.append("email", $(this).find('input[name="email"]').val());  
	    data.append("mobile", $(this).find('input[name="mobile"]').val());  
	    data.append("address1", $(this).find('input[name="address1"]').val());  
	    data.append("address2", $(this).find('input[name="address2"]').val());  
	    data.append("city", $(this).find('input[name="city"]').val());  
	    data.append("zip", $(this).find('input[name="zip"]').val());  
	    data.append("country", $(this).find('select[name="country"]').val());  
	    data.append("state", $(this).find('select[name="state"]').val());  
	    data.append("state_other", state_other);  
	    data.append("category", $(this).find('select[name="category"]').val());  
	    data.append("job_id", $(this).find('input[name="job_id"]').val());  
	    data.append("action", "submit_application_form");  

	    $.ajax({
	        type: 'POST',
	        url: settings.ajaxurl,
	        data: data,
	        contentType: false,
	        processData: false,
	        success: function(response){
            	form.find('input[name="submit"]').removeAttr('disabled');
            	form.find('.showLoaderLoader').css('display', 'none');
            	form.find('input[name="submit"]').css('opacity', '1');
				form.find('.successText').html('');
				if (response == 'Allowed file types are .doc, .pdf and .docx') {
					alert('Allowed file types are .doc, .pdf and .docx');
	            	form.find('.successText').html(' &nbsp; &nbsp; Allowed file types are .doc, .pdf and .docx');
				} else {
					$('.bh2wp-apply').hide();
					$('#successMessage').show();
					$('.bh2wp-application-form').parent().parent().removeClass('increase-height');
					form.hide();
					$('.bh2wp-check-candidate-form').show();
	            	form.find('.successText').html(' &nbsp; &nbsp; Your Application has been submitted successfully!');
					$(".bh2wp-application-form")[0].reset();
					setTimeout(function(){
						form.find('.successText').html('');
					}, 5000);
				}
	        }
	    });

	    return false;
	});

	function isEmptyOrSpaces(str){
		return str === null || str.match(/^ *$/) !== null;
	}

})( jQuery );
