var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		PRIMARY NAV
	\*----------------------------------------------------------------*/
	$("button.menu-button").click(function () {
		$(this).siblings("nav").addClass("is-active");
	});
	$('html').click(function (event) {
		if ($(event.target).closest('.navigation-block nav, button.menu-button').length === 0) {
			$('.navigation-block nav').removeClass("is-active");
		}
	});
	/*----------------------------------------------------------------*\
		NAV DROPDOWN
	\*----------------------------------------------------------------*/
	$("li.menu-item-has-children a").click(function () {
		$(this).parent().toggleClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	NAV SEARCH
	\*----------------------------------------------------------------*/
	$(".navigation-block button.search-button").click(function () {
		$('.navigation-block form').addClass("is-active");
	});
	$('html').click(function (event) {
		if ($(event.target).closest('.navigation-block form, .navigation-block button.search-button').length === 0) {
			$('.navigation-block form').removeClass("is-active");
		}
	});
	/*----------------------------------------------------------------*\
		PAGE TITLE SLIDER
	\*----------------------------------------------------------------*/
	$('.page-title.is-slider').slick({
		arrows: false,
		dots: true,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 4000,
	});
	/*----------------------------------------------------------------*\
		TESTIMONY SLIDER
	\*----------------------------------------------------------------*/
	$('.testimonials').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 2,
		autoplay: true,
		autoplaySpeed: 6000,
		responsive: [{
			breakpoint: 800,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}, ]
	});
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'table');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
	});
	/*----------------------------------------------------------------*\
		NOTIFICATION BAR
	\*----------------------------------------------------------------*/
	if (readCookie('noteClosed') === 'false') {
		$('.notification').removeClass("note-on");
	} else {
		$('.notification').addClass("note-on");
	}
	$('.notification button').click(function () {
		$('.notification').removeClass("note-on");
		createCookie('noteClosed', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
		BACK TO TOP
	\*----------------------------------------------------------------*/
	$("button.back-to-top").click(function () {
		$(window).scrollTop(0);
	});
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= 2 * $(window).height()) {
			$("button.back-to-top").addClass('is-active');
		} else {
			$("button.back-to-top").removeClass('is-active');
		}
	});
});