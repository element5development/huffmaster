<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main>
	<article>
			<?php $bg = get_field('background'); ?>
			<section class="the-great-divide cards-block" style="background-image: url('<?php echo $bg['sizes']['xlarge']; ?>');">
				<div>
					<div>
						<h2>Find Work</h2>
						<div class="cards">
							<?php while ( have_rows('find_work_cards') ) : the_row(); ?>
								<div class="card">
									<?php $link = get_sub_field('link'); ?>
									<?php $icon = get_sub_field('icon'); ?>
									<div class="icon">
										<img src="<?php echo $icon['sizes']['small']; ?>" alt="<?php echo $icon['alt']; ?>" />
									</div>
									<h3><?php the_sub_field('title'); ?></h3>
									<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"></a>
								</div>
							<?php endwhile; ?>
						</div>
						<div class="cta">
							<p>Recent Job Opportunities</p>
							<a class="button is-blue" href="/all-open-jobs/">View New Jobs</a>
						</div>
					</div>
				</div>
				<div>
					<div>
						<h2>For Clients</h2>
						<div class="cards">
							<?php while ( have_rows('for_client_cards') ) : the_row(); ?>
								<div class="card">
									<?php $link = get_sub_field('link'); ?>
									<?php $icon = get_sub_field('icon'); ?>
									<div class="icon">
										<img src="<?php echo $icon['sizes']['small']; ?>" alt="<?php echo $icon['alt']; ?>" />
									</div>
									<h3><?php the_sub_field('title'); ?></h3>
									<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"></a>
								</div>
							<?php endwhile; ?>
						</div>
						<div class="cta">
							<p>For Immediate Strike Services</p>
							<a class="button" href="tel:+18004461515">+1 (800) 446-1515</a>
						</div>
					</div>
				</div>
			</section>
			<section class="wysiwyg-block">
				<?php the_field('wysiwyg'); ?>
			</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>