<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE
	Standard page template for website which should include all the
	commonly used options.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<main>

	<article>
		
	<?php 
	if( have_rows('article') ):
		$id = 0;
		while ( have_rows('article') ) : the_row(); 
			$id++;
			if( get_row_layout() == 'wysiwyg' ):
				hm_get_template_part('template-parts/sections/wysiwyg', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'wysiwyg-two-col' ):
				hm_get_template_part('template-parts/sections/wysiwyg-two-col', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'cards' ): 
				hm_get_template_part('template-parts/sections/cards', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'link_cards' ): 
				hm_get_template_part('template-parts/sections/link-cards', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'logo_cards' ): 
				hm_get_template_part('template-parts/sections/logo-cards', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'icon_cards' ):
				hm_get_template_part('template-parts/sections/icon-cards', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'icon_links' ): 
				hm_get_template_part('template-parts/sections/icon-links', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'gallery' ): 
				hm_get_template_part('template-parts/sections/gallery', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'testimonials' ): 
				hm_get_template_part('template-parts/sections/testimonials', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'news_&_events_feed' ): 
				hm_get_template_part('template-parts/sections/news-events', [ 'sectionId' => $id ]);
			elseif( get_row_layout() == 'team_members' ): 
				get_template_part('template-parts/sections/team');
			elseif( get_row_layout() == 'newsletter' ): 
				hm_get_template_part('template-parts/sections/newsletter', [ 'sectionId' => $id ]);
			endif;

		endwhile;
	endif; 
	?>

	</article>
	
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>